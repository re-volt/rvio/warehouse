Track Name: The Keep
Folder Name: ...\levels\The_Keep
Track Type: EXTREME
Author/s: scloink & Yamada & SuPeRTaRD & The Me and Me
scls-Email: scloink@rvarchive.com
Yams-Email: NOT AVAILABLE
TaRD-Email: brgpug@ev1.net
TMaM-Email: saver@gmx.li
scls-page: http://www.rvarchive.com/ 
TMaM-page: http://www.themeandme.de/
Length: 1251 meters

Install: Unzip with file names on to the main ReVolt folder

Tools used: PSP 7.0; Glue by ali, Christer and Gabor Varga;

================================================================
Description
================================================================
Y'all know Shrek? Well we dunno if scloink really thought of it
when building this track, but we think he did. :) Really lovely
mesh and textures add to one of the most challenging Re-Volt
tracks that came out lately. One of the longest, too. You
certainly be on your toes if you want to race this one properly.
But just as certainly, you'll be having a blast when you race
it. Have fun!

================================================================
Tips
================================================================
This track was designed with/for Pro cars, so we recommend to
race the track with these. Due to the length and fluctuating
difficulty of the track as a whole, it's pretty hard to 
recommend a certain type of car.
Lap times should come to an average of about 1:30 - 1:45 with
Pro/SuperPro cars.

================================================================
Thanks And Accolades
================================================================
We want to thank you, for downloading this track, and supporting
the Re-Volt community with dlding it. Then we gotta thank the
whole RV community, too. Its probably the best community on a
racing game all around the web, and it's hard work to get there.
Now we all have to work even harder to keep it there! Thanks
and: Keep it up!

We want to thank Wayne LeMonds, for his great site and forum. If
it Wayne would not have been here, you would possibly not read
this, cuz we never would have gone so deep into Re-Volt without
the forum. Thanks a whole bunch, Wayne! If you havn't visited
his website please do so, you won't regret it.
http://www.revoltdownloads.com/

We want to thank the whole staff of RVA for their great site. It
is the perfect addition to Racerspoint and will be (or probably
is already) the completest Re-Volt site on the web, with all the
cars, tracks, tutorial, tools etc. If you don't know it yet, you
really have to hurry to get there: http://www.rvarchive.com/

We would also like to thank ALL the peeps at #re-volt chat on
the IRC austnet servers for their support and help without
forgetting anyone of them. Thanks dudes!

Then we want to thank all the creators of the tools we used.
Without their hard work, this track would not have been possible.

================================================================
Individual Thanks On This Track
================================================================
scloink:
Well, what could be said when it comes to the bloke who made the
whole mesh? :) It looks mighty fine, and we still feel honored
that you chose us to finish it. Even tho it took us and the
fellowship like one and a half years... Thanks!

Yamada:
For taking your time on trying to get the AI round the track in
a reasonable time. Due to the modelled bumps, AI aint as fast as
on flatter tracks, but it's still good. Thanks!

SuPeRTaRD:
For doing really quite a lot of work here. .vis, .fld, some of
the .pan and .taz all belong to him. Hehe... :) And we didn't
even ask you. You volunteered. Thanks bro!

Invictus artists:
For them birds and buffalos... They fit rather nicely. Into the
mouth of the dragon. Thanks!

================================================================
Copyright / Permissions
================================================================ 
You MAY distribute this TRACK, provided you include this file,
with no modifications.  You may distribute this file in any
electronic format (BBS, Diskette, CD, etc) as long as you
include this file intact.

================================================================
Where to get this TRACK
================================================================
Websites : http://www.revoltdownloads.com/
         : http://www.rvarchive.com/
	 : http://www.revoltunlimited.co.uk/
	 : http://www.themeandme.de/