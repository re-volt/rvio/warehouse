
 Sunset Hills  
 by hilaire9 - August 2013
 Length: 763m                                     
 Type: Extreme
 Folder name: sunset_hills
 Custom music, animated models and skybox with Revolt version 1.2 
 ================================================================
 * To Install: Extract into your Re-Volt folder.
 =================================================================
 Tools: 3D Studio Max 2010, ASE Tools, mkmirror, PaintShop Pro X5 
 and MAKEITGOOD edit modes. 
 hil's Home Page: http://www.hilaire9.com  
 =================================================================
 