********************************************************************************
**                               Changelog                                    **
********************************************************************************
--------------------------------------------------------------------------------

Version 20.1229a

- First Alpha release

--------------------------------------------------------------------------------
Name: Glacier Battle
Release date: 2020
Track Type: Battle Tag (Multiplayer only)
Difficulty: Extreme
--------------------------------------------------------------------------------

Notes: Batte Tag Arena originally made for the canceled Arctic Blast Mod.
Base and original arena is made by Zach.
Custom sky is made by Xarc.
GFX photo is made by Santiii727.
Custom snow properties is made by Tryxn (Zeino) and Xarc.

Special Notes:
Thanks to Zach for share this and MightyCucumber for contact and contributing this map to the re-volt I/O Workshop.

Contact Zach:  zach@revoltzone.net


©️ 2009-2020 Zach and RVGL I/O Community.
