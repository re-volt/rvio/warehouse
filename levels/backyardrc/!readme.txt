- Backyard rev1 -

Author: Marv

A track from Sierra's  3D Ultra RC Racers, 
modeled from scratch with Blender and Jig's wonderful plugin
Textures mainly ripped from screenshots, some are customly made


Features:
  - made for Re-Volt 1.2
  - compatible with earlier versions
  - custom sounds and soundtrack from the original game
  - custom pick-ups inspired from the original game
  - TVTIME camera with car sounds (TVTIME cheat, press F5 in-game)


Feel free to contact me:
  marvinthiel@me.com


Thanks to Thomas, ZR, Skarma, Pong Pearson and Kay


http://revoltfrontend.wordpress.com
http://z3.invisionfree.com/Revolt_Live/index.php?


Changelog:

- 2013-06-11 rev1
  - disabled shortcuts around hut (too unfair)
  - placed stones