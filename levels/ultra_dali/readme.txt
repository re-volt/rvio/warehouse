 
 Ultra Dali  
------------------------
 by hilaire9 - July 2010 
-------------------
 Length: 727 meters                                      
 Type: Extreme (for Revolt version 1.2 only)
==============================================================
 
 *** To Install: Unzip into your main Re-Volt folder.
 
============================================================== 
 Description: A dream caused by the flight of a bee around 
 a pomegranate one second before awakening.   
----------------------------------------------------------
 Textures: Salvador Dali.
-------------------------
 Tools/Credit: 3ds Max 5, ASE Tools, mkmirror, MAKEITGOOD 
 edit modes, and Paint Shop Pro 9. 
 Revolt version 1.2 by huki and jigebren.
------------------------------------------------------------------------

 
 
