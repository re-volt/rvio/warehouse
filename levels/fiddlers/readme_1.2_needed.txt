﻿Hey Re-Volters!




I spend a few days mapping this track, fixing some errors, completing it with some extras (birds!) and improving the raceability. 

Thomas (R6) was a great help. Credit for AI nodes, pos nodes, cam nodes, pickups and visiboxes goes to him. 
He motivated me a lot. E.g. the skyscrapers in the background would not exist without him.

I'd also like to thank Kay for helping me solving problems and answering any question.

I created this track with Jigebren's Blender plugin which works just fine. Thanks a lot for this!



I tried to keep the track as clean and original as possible to get close to Re-Volt's first concept:




QUOTE (Simon Harrison)

"I came up with the original idea along with paul phippen […] the original idea was more realistic, no weapons or speedups, 
[…]racing around the dirty walkways and dodging old mattresses and pools of piss. nice."




Consider it as you like; the track got very heavy. 
You might want to play it in a lower resolution if you haven't got at least a dual core 2.5 GHZ processor. 
We tried our best to keep the FPS high.



You will definitely need Re-Volt 1.2 with custom support (get the update here, download the newest alpha: http://rv12.zackattackgames.com/downloads.php) to play this track.





I hope you like it! Have fun!


-Marv alias MarvTheM (http://marvtsblog.wordpress.com/)




--Stats--


-17858 polies

-8931 of those are solid

-8 days of work (mapping and makeitgood)

-Original mesh taken from the Re-Volt source code (RV_MESHES/Roof/Roof.3ds)

-Soundtrack: Re-Volt N64 "Recharge"