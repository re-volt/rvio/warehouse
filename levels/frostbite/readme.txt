      /������/ /����\  /����\  /����\    /����\  /�������/ /�������/ /������/
     / _____/ / /�/ / / /�/ / / /�\_/   / /�/ / /__  ___/ /__  ___/ / _____/ 
    / /      / /_/ / / / / /  \ \      /  �__/    / /       / /    / /       
   / ===/   /   __/ / / / /    \ \    /    \     / /       / /    / ===/    
  / /      / /| |  / / / / _    \ \  / /�/ /    / /       / /    / /         
 / /      / / | | / /_/ / / \___/ / / /_/ / /���  ��/    / /    /  ����/     
/_/      /_/  |_| \____/  \______/ /_____/ /_______/    /_/    /______/      

---------------------------FROSTBITE -> README.txt---------------------------

Track  :	Frostbite
Author :	Zach K. (zagames)
Folder :	frostbite
Length :	614 meters
Skill  :	Medium
Type   :	Extreme / Full Custom

---------------------------FROSTBITE -> README.txt---------------------------

Notes  :	Full Custom is a track type that adds custom models, sounds,
			etc. into otherwise normal tracks.  Phoenix R3 is
			required to race this track as a Full Custom, but only
			Re-Volt is required to race normally.  If you wish to
			include parts of this track in your own, feel free to
			do so, but please give me credit in your README file.

---------------------------FROSTBITE -> README.txt---------------------------

Tools  :	3DS Max 8
		(Microsoft) Paint
		RVTMOD7 (ase2w, ase2prm, rvglue)
		Re-Volt (MAKEITGOOD)	 

---------------------------FROSTBITE -> README.txt---------------------------

Credits:	RST for his duck.prm, Phoenix R3, and testing
		Hilaire9 for some textures and testing
		TSO for the background music (Wizards in Winter)
		The creators of RVTMOD7
		Jesus
		Davegh for some textures
		And many others for help testing

---------------------------FROSTBITE -> README.txt---------------------------

Website:	http://revolt.zackattackgames.com		(Mine)
		http://revolt.speedweek.net/main/news.php	(RVZT)
		http://z8.invisionfree.com/RRR_Racing_Forum	(Phoenix R3)

---------------------------FROSTBITE -> README.txt---------------------------

Misc.  :	Wizards in Winter is (C) Trans-Siberian Orchestra