Wabe Ring by Zeino (Tryxn on RVZ)

this is entry for my own competition: https://forum.re-volt.io/viewtopic.php?f=20&t=1366
the track is using one texture page for all textures, its short and narrow.
featuring:
- time trial time to beat
- reverse mode
- and more fun stuff

special thanks:
javildesign (Jan) for the reworked road and tips
other authors of tracks i took the textures from (saffron, alexander,..)

please enjoy