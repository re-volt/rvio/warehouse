﻿Content:
>>> Install Hanging Gardens Battle
>>> Track information
>>> How to contact me
>>> Description
>>> Known issues
>>> Tools used
>>> Thanks and credits
>>> Disclaimer

Install Hanging Gardens Battle:
Unzip Hanging Gardens Battle into the main folder of the game.

Track information:
>>> Name: Hanging Gardens Battle
>>> Folder name: bab_bat
>>> Author: Keyran
>>> Development time: February 2024 - April 2024
>>> Year of first release: 2024
>>> Category: Battle Tag Arena
>>> Difficulty: Hard
>>> Stars: 10
>>> Speedups: 0

Contact:
On Discord: I am keyran_rv.

Description:
The standard track version of Hanging Gardens doesn't fully use its track layout.
Most of the pickups have new locations, often on parts that are not used in the standard track.

Known issues:
None.

Tools used:
>>> Photofiltre for textures
>>> Blender with Marv's plugin.
>>> RVGL (Makeitgood)
>>> WorldCut
>>> Visual Studio Code for custom animations and editing the inf file.

Thanks and credits:
>>> Acclaim and Probe for the original game.
>>> Textures and models:
Several textures and models come from Wonderful Skylands tracks by Xarc (some have been edited).
The fire animation comes from Medieval: Redux by Instant and Kipy and was originally made by Kiwi.
babylone.bmp is in public domain.
The skybox is in public domain, downloaded from https://polyhaven.com/ (reused from Isivolt 2).

>>> Music:
Theme by Darkfluxx, licence CC BY, from Free Music Archive.

>>> Misc:
I have reused a material and a dust type from Spa-Volt 1 by Kiwi.

Disclaimer:
Don't use this track or its content for commercial purposes.
You can reuse any part of this track provided you mention the original authors. 