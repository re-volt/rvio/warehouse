{
	MATERIAL {
	  ID              15
	  Name            "CANAL"
	  Skid            true
	  Spark           false
	  Corrugated      true
	  Moves           false
	  Dusty           true
	  Roughness       1
	  Grip            0.9
	  Hardness        1
	  SkidSound       6
	  ScrapeSound     5
	  SkidColor       200 200 200
	  CorrugationType 2
	  DustType        1
}

DUST {
	  ID              1
	  Name            "CANAL"
	  SparkType       28
	  ParticleChance  5
	  ParticleRandom  0
	  SmokeType       30
	  SmokeChance     1
	  SmokeRandom     0.1
	}
}