===================================================================================
Track Name--- Backyard Bash

Author---         Dave-o-rama 

Type---   Extreme

Length---       829m

Directions---    Unzip in the main Re-volt folder, then everything should be set.

Description--- A total backyard party! R/C Cars everywhere racing around the whole yard, shootin' the mess out of eachother.

Thanks---  To JimK for his OFFROAD kit, Acclaim for prms and textures, those responsible for rvtmod7, And you for downloading.

Copyrights\Permissions:

This track is (c) 2008 by Dave-o-rama.

You MAY distribute this TRACK, provided you include this file,
with no modifications.  You may distribute this file in any
electronic format (BBS, Diskette, CD, etc) as long as you
include this file intact.

If you have any questions about this track, here is an E-mail address to reach me.

My E-mail: davedontalk@gmail.com

: )

==================================================================================
P.S. Don't fall into the pit of death or fly into the doghouse. It smells in there. Trust me, I would know. heh heh :)