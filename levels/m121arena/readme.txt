Track name: The Arena 
Install in folder: Re-volt\levels\M121Arena
Author: Mace (aka: Mace121)
Email Address: Macethe0mni@gmail.com

Description:
This is an old map, and my first level made entirely using Blender before the Klashers 888 track. This is Mace121's The Arena, an old lonely hockey rink in the middle of nowhere, and you're caged in with your opponents, gladiator style. There are two stars located on both sides of the arena, and powerups peppered all over designated spots. It's a no holds bars match of the ages.
______________________________________________________________________________________

Some Info:

-It's a Battle Tag map
-There's nowhere to run
-There's nowhere to hide
-Despite being a hockey rink, it's awfully icy.
	-Adding to the icy surface, oil slicks are much more effective.


Misc.

This was made in around March 16th 2018 - April 1st 2018, as simply testing the waters of making a track from the ground up. Level's inspired by my local hockey rink in town, which was actually placed beside the skate park. 
______________________________________________________________________________________

* Copyright / Permissions *

Yeah, sure. Why not, unless you credited the author(s).
