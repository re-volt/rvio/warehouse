Information
-----------------
Track Name:	Daybreak Raceway ALPHA
Length:		856 meters
Difficulty	Medium
Author:		Saffron


Description
-----------------
A quick racetrack made with a bezier curve, and slightly extended with some eyecandy.

This is not the final product - but I wanted to release it as it is, as I feel it's pretty good despite the lack of features. The graphics and the raceline will change (potentially three variations), alongside elevation changes.


Requirements
-----------------
You MUST use the latest RVGL patch for the custom properties to work.


Credits
-----------------
The Internet, the RVTT team and Polyphony Digital for the textures
Hipshot for the skybox
The Blender Foundation for Blender
Marv for his Blender plug-in
Huki and Marv (again) for RVGL
Rick Brewster for Paint.NET
Victor for testing out of the track and giving feedback
Everyone in Re-Volt Discord


Permissions
-----------------
This track may not be distributed anywhere else outside of Re-Volt Zone and Re-Volt I/O content packs.