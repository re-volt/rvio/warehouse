

-------------------------------------------------------------------SS Route Re-Volt--------------------------------------------------------------------------

----------------------------------------------------------------------by Crescviper----------------------------------------------------------------------------------



General nformation:
------------

Name:          SS Route Re-Volt

Author:         Crescviper

Type:            Extreme

Length:         510 m.

Difficult:        8/10 (the AI nodes are very good)

Surface:       The surface installed by me with ASE TOOLS are: The Default Surface and the Gravel Surface (in -morph mode)

-------------
Installation:
-------------

You must unzip the Citt�.zip into your main Re-Volt directory.

------------
Descrizione (italiano):
------------

Questa � la mia seconda pista fatta con gli ASE TOOLS

La pista � ambientata in una citt� metropolitana dove le auto di Re-Volt possono sfrecciare per le strade alla massima velocit�

Per questa pista mi sono ispirato al tracciato "Special Stage Route 5" del gioco Gran Turismo 4

------------------------------------------------------------------------------------------------------------------
------------

Description (English):
------------

This is my second track that i made in re-volt with ASE TOOLS

The track is set in a metropolitan city where cars of Re-Volt can run through the streets at full speed.

For this track I was inspired by the track "Special Stage Route 5" of the game Gran Turismo 4.

----------------------------------------

Programs used by me for make this track:

----------------------------------------

3dsmax
ASE TOOLS
Makeitgood
RV-SHADE
Zmodeler
Paint of windows XP
Wolfr4 by Jigebren (to enable the skymap and the MP3)

---------------------------------------

Special thanks to the italian site "Aliasrevoltmaster" and the english site "RVZT"


Copyright by Crescviper 2011...All right reserved
