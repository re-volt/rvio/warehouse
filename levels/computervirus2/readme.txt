An extreme track by Mladen007
=Track description=
Track name :Computer Virus 2
Length:670 metres
=What i used to build this=
Paint.net,Blender and MAKEITGOOD cheat
=Credits=
To Dave-o-rama for the first Computer virus.
To Marv for teaching me track stuff, for shading the track,making custom assets and for tutorials he wrote/collected on his site (http://re-volt.io/).
To the authors of their respective tutorials mentioned above.
To people that made Paint.net.
To Jigebren for his blender plugin.
=Permision=
You can do whatever you want with the track but you must give me credits.
=Additional Info=
Latest RVGL is recommended.
Link to 17.0327 (latest stable RVGL at the time of writing this readme) -> http://rv12.revoltzone.net/rvgl.php
Link to the forum relevant to the RVGL developement -> http://z3.invisionfree.com/Our_ReVolt_Pub/index.php?

If you find any bugs, do report in comment section.

Enjoy!