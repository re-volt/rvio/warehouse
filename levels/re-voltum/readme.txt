
Track info
----------
Track name:             Re-voltum
Track length:           910 m
Author:                 Toffe
Type :       		Extreme 
zip file size :		2,3 Mo

Description
-----------
This is a kind of reconstitution of a roman city with its circus, its thermae, ...


Installation / uninstallation
-----------------------------

Install folder: Re-voltum
Installation:   unzip Re-voltum.zip into your Re-Volt directory.
Uninstallation: delete <Re-Volt dir>\levels\Re-voltum
                delete <Re-Volt dir>\gfx\Re-voltum.bmp
                delete <Re-Volt dir>\gfx\Re-voltum.bmq


Legals
------
I used somme textures from "Mikes Medievel Mahem" by Skitch2 and some instances and 
textures from "Venice" by Gabor Varga.
You can use, copy and distribute this package; you can use any part of it
(meshes, textures) unchanged or modified in your own work - provided you
do it for free.


Acknowledgements
----------------
Thanks to 	Acclaim for making this wonderful game.
		Ali, AntiMorph, and Gabor for ASE tools
		the prm kits authors
		you for downloading it...
		and My girldfriend for her help and her patience ...
Happy racing,
Toffe
