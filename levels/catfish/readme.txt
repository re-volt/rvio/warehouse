 
 Catfish Cove 
 -----------------------------
 by hilaire9 --- February 2007
 ------------------
 Length: 995 meters                                      
 Type: Extreme
 ====================================================
 
 *** To Install: Unzip into your main Re-Volt folder.
 
 =====================================================
 * Description: A long race around Catfish Cove. Don't 
 forget to stop by Ricky's Bait and Tackle shop for 
 some nightcrawlers and beer. The catfish are biting 
 on the south side of the lake early in the morning. 
 ----------------------------------
 Textures from Urban Terror 4.1
 ----------------------------------
 * Tools/Credit: 3ds Max 5, ASE Tools, MAKEITGOOD edit modes,
 and Paint Shop Pro 9. 
 ------------------------------------------------------------
 --- hilaire9's Home Page: http://members.cox.net/alanzia 

 
 

