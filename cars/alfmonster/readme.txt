August 09, 2017: Readme file of Beta.

================================================================
======================== Car Info ==============================
================================================================

Name				Beta
Author				Allan1
Date				January 08, 2014
Type				Original
Folder Name			alfmonster
Rating				Amateur
Class				Glow
Drivetrain			Four-wheel Drive
Top speed			34 mph
Body mass			2.3 kg


================================================================
========================= Setup ================================
================================================================

Extract the zipped file into your Re-Volt directory. Simple as that.


================================================================
======================= Description ============================
================================================================

A black monster truck with red decals. Really soft parameters.
It was intended to be the second version of another car of mine: Alpha (also known as 0.8 Alpha).
Body was based on BigVolt a bit, and wheels were borrowed from Pix Skull.


================================================================
======================== Credits ===============================
================================================================

Decals (font) by David Kerkhoff.

Track in the preview picture is Aztec by Hilaire9 and Plic.

Tools:
Autodesk 3D Studio Max 2010
Asetools (by Ali and Kay)
Adobe Photoshop CS2
CorelDRAW x3
Prm2Hul (By Jig)
Re-Volt 1.2
MS Notepad


================================================================
======================= Changelog ==============================
================================================================

October 15, 2016:
-Fixed carbox
-Fixed texture

August 09, 2017:
-Fixed axles
-Fixed body AngRes


================================================================
=================== Permissions and terms ======================
================================================================

(This creation don't have a license. Even so, please be polite and respect the author wishes)

You're free to share (copy and redistribute the material in any medium or format) and adapt (remix, transform, and build upon the material) this creation, provided that:

1. You give appropriate credit and indicate if changes were made. You may do so in any reasonable manner.

2. You'll not use the material for commercial purposes.

3. If you remix, transform, or build upon the material, you must distribute your contributions under the same terms/permissions as the original.

4. You may not apply legal terms or technological measures that legally restrict others from doing anything the author permits.


================================================================
======================= Final notes ============================
================================================================

Please contact me if you find bugs or if you have any suggestion or idea. You can do so at the comments section of Re-Volt Zone or Dropbox, or even with a private message at the forum Re-Volt Live or via email.

My email adress: allanmoraes27@yahoo.com

Re-Volt Live profile: http://z3.invisionfree.com/Revolt_Live/index.php?s=90eae8a917757853e685db4ff40ec0a2&showuser=486



For more, visit my website: http://allanhqs.wix.com/warehouse


Have a nice Race!

Allan