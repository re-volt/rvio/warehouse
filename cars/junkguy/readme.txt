August 10, 2017: Readme file of Dump Truck


================================================================
======================== Car Info ==============================
================================================================

Name			Dump Truck
Author			Allan1
Date			April 09, 2013
Type			Remodel
Folder Name		junkguy
Rating			Rookie
Class			Electric
Drivetrain		Four-wheel Drive
Top speed		31 mph
Body mass		2.3 kg


================================================================
========================= Setup ================================
================================================================

Extract the zipped file into your Re-Volt directory.

There's an alternative blue/white skin inside. To use it, just open parameters.txt and change the TPAGE line to "cars\junkguy\caror.bmp"



================================================================
======================= Description ============================
================================================================

That's a car (or truck?) based on RC Revenge's Big Rock and the truck model from Toy World levels. It has an average speed and acceleration. Springs are quite bouncy.


================================================================
======================== Credits ===============================
================================================================

RC Revenge's Big Rock and the truck model from Toy World were used as base (as mentioned above). Both owned by Acclaim.

Track in the picture is Arena Extreme by Bak95.

Tools:
Autodesk 3D Studio Max 2011
Asetools (By Ali)
Adobe Photoshop CS2
MS Paint
Re-Volt 1.2 (By Huki and Jig)
CorelDRAW x3
CorelPHOTO-PAINT X3
Prm2Hul (By Jig)
Zmodeler
MS Notepad


================================================================
======================== Changelog =============================
================================================================

October 30, 2015:
-Changed name
-Fixed body's alignment
-Fixed aerial's offset
-Added an alternative skin
-Decreased top speed (moved to Rookies)
-Added axles

November 25, 2016:
-Increased top speed to 31 mph (28 before)

August 10, 2017
-Changed body AngRes
-Fixed axles


================================================================
=================== Permissions and terms ======================
================================================================

(This creation don't have a license. Even so, please be polite and respect the author wishes)

You're free to share (copy and redistribute the material in any medium or format) and adapt (remix, transform, and build upon the material) this creation, provided that:

1. You give appropriate credit and indicate if changes were made. You may do so in any reasonable manner.

2. You'll not use the material for commercial purposes.

3. If you remix, transform, or build upon the material, you must distribute your contributions under the same terms/permissions as the original.

4. You may not apply legal terms or technological measures that legally restrict others from doing anything the author permits.


================================================================
======================= Final notes ============================
================================================================

Please contact me if you find bugs or if you have any suggestion or idea. You can do so at the comments section of Re-Volt Zone or Dropbox, or even with a private message at the forum Re-Volt Live or via email.

My email adress: allanmoraes27@yahoo.com

Re-Volt Live profile: http://z3.invisionfree.com/Revolt_Live/index.php?s=90eae8a917757853e685db4ff40ec0a2&showuser=486



For more, visit my website: http://allanhqs.wix.com/warehouse


Have a nice Race!

Allan