Car information
================================================================
Car name        	: Burning Sensation

Car Type  		: Original

Top speed 		: 42mph (theoretical) / 62kph (observed)

Rating/Class   		: 4 (pro)

Installed folder       	: ...\cars\burningsense

Description           	: Another military type vehicule.
                        : This time, entirely made by me (mapping is terrible !).
			: The wheels are based on those in case you wonder : http://www.conrad.fr/ce/fr/product/238072/Pneus-complets-Reely-Alu-Fighter-18?ref=list
 

Author Information
================================================================
Author Name 	: Z3R0L33T and mightycucumber

Email Address   : z3r0l33t@hotmail.fr

Other Info	: ReVolt user since 1999 !



Construction
================================================================
Base           	: Original concept car

Editor(s) used 	: Blender, ZModeler 1.07, Paint.net


 
Additional Credits 
================================================================
Thanks to the whole community who continues to make ReVolt a fun and attractive game !
Special thanks to mightycucumber who helped me finish the car and give it a decent look but also thanks to ElectricBee for the logo (which I finally got to use) and all other users who came by to say hello or to give a hand.

The car's name comes both from a weapon in Blacklight Retribution and burner94, another RV-Live member who helped me with my first creations. Dude, the next car (if there is one) will be a street car. :D

Includes two slightly different wheels and two slightly different bodies.
If you want to use the sets of wheels correctly, you might want to modify the params :
"rounded" wheel Radius	20.500000
"flat" wheel Radius	19.000000



Copyright / Permissions
================================================================
You may do whatever you want with this car but you have to give us credits. Do not distribute without authorization.
 
