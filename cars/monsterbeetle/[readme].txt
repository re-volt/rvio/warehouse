Car information
================================================================
Car name: Monster Beetle
Car Type: Remodel
Top speed: 55 kph
Rating/Class: 2 (Advanced)
Installed folder: ...\cars\monsterbeetle
Description: 

After the Blackfoot, this is the second car I've made for Re-Volt.

It's the Tamiya Monster Beetle, which was originally released in 1986. In 2016, the Mall Crawler was re-released,
but some details was changed (for example the BFGoodrich-brand). I've decided to do a remake of the Original 
'86-version.

As a basis, I took the classical Baja Bug from scloink (which was also the basis for Badjam). I adjusted or
made details, like a new front bumper, back bumper, circle headlights, added shocks, and so on. Iron Bob was so kind, to
make some AWESOME wheels for Monster Beetle. They look incredible. I can't thank Iron Bob enough for them. ;) 
The parameters are completly new, it have nothing in common with Baja Bug or Badjam. I took the parameters from Blackfoot,
and added some values from Bertha Ballistics. In the end, I tried to find a good combination, so the car drives well with
his RWD drive, which was not so easy, but I think it worked fine in the end.

As written above, the car has a rear wheel drive. The Original Tamiya model has as well, and I wanted to do a proper
replica of this. Also the power of the car is not too high, so it fits the original. I would put the car into the Slugs-
Class. With a little bit of training, it's also possible to win races against Aquasonic, Matra XL, R6 Turbo or Panga TC.

It's a lot of fun to do races against Blackfoot with it. Blackfoot is faster, but Monster Beetle has a better steering and
overall handling. In testing phase, I had some awesome and funny fights with this 2 cars. This was also one of my goals.
Both cars was released in the same year by the same manufacturer (Tamiya), I'm sure a lot of people will have some childhood
memories doing fights Blackfoot vs. Monster Beetle. 

I wish you a lot of fun with Monster Beetle! :)

Author Information
================================================================
Author Name: Kiwi
Email Address: ingenieur.kiwi@gmail.com
Other Info: I own Re-Volt since 1999. I'm a casual driver, and not the best in it. I race just for fun, and
Re-Volt is the perfect game for this. I'm new to modelling - for Blackfoot I worked the first time with a 3D
modelling software. But I have some texturing knowledge - I made skins for the game TOCA2 und TOCA Race Driver
in the beginning of the 2000s.
 
Construction
================================================================
Base: Baja Bug (Body) from scloink, Original wheels from Iron Bob
Editor(s) used: Notepad++, Blender (with HabitatB- and ReVolt-Plugin from Marv), PhotoImpact12
 
Additional Credits 
================================================================
Thanks to the guys at RVIO community in Discord (#creations) for your help, suggestions and compliments!
Mladen and Gotolei for helping me with the HUL file.
Special thanks to Marv, you are my personal Blender genius.
Thank you for test drives and thoughts:
 - URV
 - Matsilagi
 - Trixed
 - Iron Bob
 - Santiago
 - Javes
Stone for his interest in test driving
Iron Bob for this AWESOME wheels - without them, the car would maybe only look half that good. Thanks a lot! :)

Copyright / Permissions
================================================================
You may do whatever you want with this car, as long as you mention me in the credits.

HAVE FUN!

Kiwi

Version 1.0 from October 4th, 2017