﻿Car information
================================================================
Car name                : Heatroad
Car Type  		: Repaint
Top speed 		: 31 mph
Rating/Class   		: Rookie
Installed folder       	: ...\cars\heatroad

Author Information
================================================================
Author Name 		: Xarc
Email Address           : warrockrockwar@hotmail.it
 
Construction
================================================================
Base           		: Ford Deluxe (by Saffron)
Editor(s) used 		: MediBang Paint Pro
 
Additional Credits 
================================================================
Credits to Saffron for the original car

Copyright / Permissions
================================================================
You may do whatever you want with this CAR.