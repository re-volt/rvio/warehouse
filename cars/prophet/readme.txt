Car information
================================================================
Car name        	: Prophet

Car Type  		: Original

Top speed 		: 46mph (theoretical) / 70kph (observed)

Rating/Class   		: 4 (pro)

Installed folder       	: ...\cars\prophet

Description           	: A brand new concept car with a turbine.
                        : Completely modeled, mapped and textured by me.
			: This is powered by the front wheels. It has a poor acceleration but an overall nice topspeed.
 

Author Information
================================================================
Author Name 	: Z3R0L33T

Email Address   : z3r0l33t@hotmail.fr

Other Info	: ReVolt user since 1999 !



Construction
================================================================
Base           	: Original concept car

Editor(s) used 	: Blender, ZModeler 1.07, Paint.net


 
Additional Credits 
================================================================
Thanks to the whole community who continues to make ReVolt a fun and attractive game !

This car's name has no specific meaning (seriously, I just hit randomly my keyboard and rearranged the letters).

It includes 3 skins with different colors.



Copyright / Permissions
================================================================
You may do whatever you want with this car but you have to give us credits. Do not distribute without authorization.
 
