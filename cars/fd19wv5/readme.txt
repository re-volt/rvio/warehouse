﻿Car information
================================================================
Car name                : WV-5
Car Type  		: Remodel
Top speed 		: 35.8 mph
Rating/Class   		: Advanced
Installed folder       	: ...\cars\fd19wv5

Author Information
================================================================
Author Name 		: Xarc
Email Address           : warrockrockwar@hotmail.it
 
Construction
================================================================
Base           		: Candy Pebbles
Editor(s) used 		: Blender and MediBang Paint Pro
 
Additional Credits 
================================================================
Credits to jigebren for the Blender plugin and prm2hul

Copyright / Permissions
================================================================
You may do whatever you want with this CAR.