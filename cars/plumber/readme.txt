August 14, 2016: Readme file of PhantoMa.

================================================================
======================== Car Info ==============================
================================================================

Name				PhantoMa
Author				Allan1
Date				September 23, 2013
Type				Remodel
Folder Name			plumber
Rating				Pro
Class				Glow
Drivetrain			Four-wheel Drive
Top speed			44 mph
Body mass			2 kg


================================================================
========================= Setup ================================
================================================================

Extract the zipped file into your Re-Volt directory. Simple as that.


================================================================
======================= Description ============================
================================================================

A recreation of my very first original car, which was based on a hot wheels' called Way 2 Fast. It uses remodeled parts of my own car Pix Skull. It is fast and somewhat slippery at curves, and very big.


================================================================
======================== Comments ==============================
================================================================

-Fast history about the car's design:

W2-Fast (first name) was my very first original, after years creating ugly repaints and undrivable cars. However, I had troubles with materials, texture mapping and exporting, so I left it laying down on my sorage. Thanks to kay, I've learned where I was failing, and I was able to fully finished it in December 18, 2012. The oldest version is available on my website, if you're interested.

One year after, I discovered so many things about car making, and by consequence, I've decided to recreate my W2-Fast using the parts of my car Pix Skull, after some months releasing it. This new version was named Ghostly Machine. The first model was too high poly and had some bugs in the mesh, hence why I decided to remake it.

It still layed on my HD by three years, and just now I have release it, with the name of PhantoMa (junction of the words 'Phantom' and 'Machine').

So, for a better clarification, here's the time line:

Way 2 Fast (first 3d model): December 17, 2011
W2-Fast (completed): December 18, 2012
Ghostly Machine (remake with Pix Skull parts): September 23, 2013
PhantoMa (small fixes before release): October 18, 2016

================================================================
======================== Credits ===============================
================================================================

Credits comes to Hot Wheels for the car design.

Track in the preview picture is Amarna by Spaceman.

Tools:
Autodesk 3D Studio Max 2009
Blender 2.66a
Re-Volt plugin for Blender (by Jig)
Asetools (By Ali)
Re-Volt 1.2
Prm2Hul (By Jig)
Adobe Photoshop CS2
MS Paint
MS Notepad


================================================================
======================== Changelog =============================
================================================================

October 18, 2016:
-Changed name
-Remade carbox
-Fixed bitmap bug

March 01, 2017:
-Changed axles inclination to the correct position

August 14, 2017:
-Fixed body AngRes and center of mass
-Fixed body and spring models offset
-Fixed axles
-Changed springs and wheels values to fix some bugs


================================================================
=================== Permissions and terms ======================
================================================================

(This creation don't have a license. Even so, please be polite and respect the author wishes)

You're free to share (copy and redistribute the material in any medium or format) and adapt (remix, transform, and build upon the material) this creation, provided that:

1. You give appropriate credit and indicate if changes were made. You may do so in any reasonable manner.

2. You'll not use the material for commercial purposes.

3. If you remix, transform, or build upon the material, you must distribute your contributions under the same terms/permissions as the original.

4. You may not apply legal terms or technological measures that legally restrict others from doing anything the author permits.


================================================================
======================= Final notes ============================
================================================================

Please contact me if you find bugs or if you have any suggestion or idea. You can do so at the comments section of Re-Volt Zone or Dropbox, or even with a private message at the forum Re-Volt Live or via email.

My email adress: allanmoraes27@yahoo.com

Re-Volt Live profile: http://z3.invisionfree.com/Revolt_Live/index.php?s=90eae8a917757853e685db4ff40ec0a2&showuser=486



For more, visit my website: http://allanhqs.wix.com/warehouse


Have a nice Race!

Allan