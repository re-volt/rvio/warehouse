{

;============================================================
;============================================================
; Mad Cat Z
;============================================================
;============================================================
Name      	"Mad Cat Z"


;====================
; Model Filenames
;====================

MODEL 	0 	"cars\mcz\body.prm"
MODEL 	1 	"cars\mcz\wheelfl.prm"
MODEL 	2 	"cars\mcz\wheelfr.prm"
MODEL 	3 	"cars\mcz\wheelbl.prm"
MODEL 	4 	"cars\mcz\wheelbr.prm"
MODEL 	5 	"NONE"
MODEL 	6 	"NONE"
MODEL 	7 	"NONE"
MODEL 	8 	"NONE"
MODEL 	9 	"NONE"
MODEL 	10 	"NONE"
MODEL 	11 	"NONE"
MODEL 	12 	"NONE"
MODEL 	13 	"NONE"
MODEL 	14 	"NONE"
MODEL 	15 	"NONE"
MODEL 	16 	"NONE"
MODEL 	17 	"cars\misc\Aerial.m"
MODEL 	18 	"cars\mcz\AerialT.m"
TPAGE 		"cars\mcz\car.bmp"
TCARBOX		"cars\mcz\carbox.bmp"
TSHADOW		"cars\mcz\shadow.bmp"
SHADOWTABLE	-60.8 60.8 64.6 -61.4 -2.5
COLL 	"cars\mcz\hull.hul"
EnvRGB 	120 120 120

;====================
; Stuff mainly for frontend display and car selectability
;====================

BestTime   	TRUE
Selectable 	TRUE
Statistics	TRUE
Class      	0 			; Engine type (0=Elec, 1=Glow, 2=Other)
Obtain     	4 			; Obtain method
Rating     	3 			; Skill level (rookie, amateur, ...)
TopEnd     	3390.000000 			; Actual top speed (mph) for frontend bars
Acc        	6.205100 			; Acceleration rating (empirical)
Weight     	1.600000 			; Scaled weight (for frontend bars)
Handling   	50.000000 			; Handling ability (empirical and totally subjective)
Trans      	1 			; Transmission type (calculate in game anyway...)
MaxRevs    	0.500000 			; Max Revs (for rev counter)

;====================
; Handling related stuff
;====================

SteerRate  	3.000000 			; Rate at which steer angle approaches value from input
SteerMod   	0.400000 			;
EngineRate 	3.500000 			; Rate at which Engine voltage approaches set value
TopSpeed   	38.800000 			; Car's theoretical top speed (not including friction...)
DownForceMod	2.000000 			; Down force modifier when car on floor
CoM        	0.000000 -12.000000 -4.000000 		; Centre of mass relative to model centre
Weapon     	0.000000 -32.000000 64.000000 		; Weapon genration offset

;====================
; Car Body details
;====================

BODY {		; Start Body
ModelNum   	0 			; Model Number in above list
Offset     	0, 0, 0 		; Calculated in game
Mass       	1.600000
Inertia    	1120.000000 0.000000 0.000000
           	0.000000 2290.000000 0.000000
           	0.000000 0.000000 1410.000000
Gravity		2200 			; No longer used
Hardness   	0.000000
Resistance 	0.001000 			; Linear air esistance
AngRes     	0.001000 			; Angular air resistance
ResMod     	29.000000 			; Ang air resistnce scale when in air
Grip       	0.010000 			; Converts downforce to friction value
StaticFriction 0.800000
KineticFriction 0.400000
}     		; End Body

;====================
; Car Wheel details
;====================

WHEEL 0 { 	; Start Wheel
ModelNum 	1
Offset1  	-28.500000 11.000000 41.000000
Offset2  	-4.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	FALSE
SteerRatio  	1.000000
EngineRatio 	16000.000000
Radius      	13.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	10.000000
SkidWidth   	15.000000
ToeIn       	0.000000
AxleFriction    	0.020000
Grip            	0.014000
StaticFriction  	1.720000
KineticFriction 	1.620000
}          	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum 	2
Offset1  	28.500000 11.000000 41.000000
Offset2  	4.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	FALSE
SteerRatio  	1.000000
EngineRatio 	16000.000000
Radius      	13.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	10.000000
SkidWidth   	15.000000
ToeIn       	0.000000
AxleFriction    	0.020000
Grip            	0.014000
StaticFriction  	1.720000
KineticFriction 	1.620000
}          	; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum 	3
Offset1  	-28.500000 11.000000 -37.400000
Offset2  	-4.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	TRUE
SteerRatio  	0.280000
EngineRatio 	16000.000000
Radius      	13.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	10.000000
SkidWidth   	15.000000
ToeIn       	0.000000
AxleFriction    	0.050000
Grip            	0.017000
StaticFriction  	1.980000
KineticFriction 	1.780000
}          	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum 	4
Offset1  	28.500000 11.000000 -37.400000
Offset2  	4.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	TRUE
SteerRatio  	0.280000
EngineRatio 	16000.000000
Radius      	13.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	10.000000
SkidWidth   	15.000000
ToeIn       	0.000000
AxleFriction    	0.050000
Grip            	0.017000
StaticFriction  	1.980000
KineticFriction 	1.780000
}          	; End Wheel


;====================
; Car Spring details
;====================

SPRING 0 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	500.000000
Damping     	11.000000
Restitution 	-0.950000
}           	; End Spring

SPRING 1 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	500.000000
Damping     	11.000000
Restitution 	-0.950000
}           	; End Spring

SPRING 2 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	500.000000
Damping     	11.000000
Restitution 	-0.950000
}           	; End Spring

SPRING 3 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	500.000000
Damping     	11.000000
Restitution 	-0.950000
}           	; End Spring


;====================
; Car Pin details
;====================

PIN 0 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 1 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 2 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 3 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin


;====================
; Car axle details
;====================

AXLE 0 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 1 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 2 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 3 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle


;====================
; Car spinner details
;====================

SPINNER {   	; Start spinner
ModelNum    	-1
Offset      	0.000000 -55.000000 32.000000
Axis        	0.000000 1.000000 0.000000
AngVel      	0.000000
}           	; End Spinner


;====================
; Car Aerial details
;====================

AERIAL {    	; Start Aerial
SecModelNum 	17
TopModelNum 	18
Offset      	0.000000 -14.000000 -4.500000
Direction   	0.000000 -1.000000 0.000000
Length      	16.000000
Stiffness   	2000.000000
Damping     	5.500000
}           	; End Aerial


;====================
; Car AI details
;====================

AI {        	 ;Start AI
UnderThresh 	150.000000
UnderRange  	1500.000000
UnderFront	 	450.000000
UnderRear   	335.000000
UnderMax    	0.950000
OverThresh  	100.000000
OverRange   	1391.000000
OverMax     	1.000000
OverAccThresh  	10.000000
OverAccRange   	400.000000
PickupBias     	16383
BlockBias      	16383
OvertakeBias   	16383
Suspension     	0
Aggression     	0
}           	; End AI

}

77173A92