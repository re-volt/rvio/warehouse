////// DESCRIPTION //////

Crytocerox is a combination between Kipy's Kookaburra (Rotor remodel) and StrixMidnight's Rhino Roadster K2 (Rotor repaint). The model has been resized and remapped, while the texture has been made twice as big and went through a few small changes, especially for the rear of the car (the bird logo you can see there is, indeed, a kookaburra).

The name of the car is an amalgamation between "Rhinoceros" and "Clytoceyx Rex", which is the scientific name for the shovel-billed kookaburra.

Crytocerox distinguishes itself through its high acceleration and excellent cornering ability, which makes up for its inferior top speed. The car may prove difficult to master, but it can provide a challenge even to Pro cars when handled skillfully.

////// ATTRIBUTES //////

CLASS:	Glow
RATING:	Semi-Pro
SPEED:	36 MPH
ACCEL.:	5.1 seconds
WEIGHT:	1.6
TRANS.:	4WD


////// TOOLS USED //////

- Blender
- Paint.NET
- waifu2x

////// CREDITS //////

I would like to express my gratitude to StrixMidnight, Kipy and fr13ndz0n3dguy for providing the excellent resources that led to the creation of Crytocerox.

Many thanks to Marv, Skarma, Kipy and Vaid for providing tips and/or feedback.

////// NOTES //////

This custom car has been created by URV.

You may use this car as a base for any future creations, as long as you give credit where is due (including credit to the authors of the original resources).

As of the latest version, this car can be found on Re-Volt Zone (revoltzone.net) or Re-Volt I/O (creations.re-volt.io). If this car has been downloaded from anywhere else, it likely has not been uploaded by the original author and may include unintended differences.

The latest version of this car is from June 16, 2017.