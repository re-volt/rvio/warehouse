Car information
================================================================
Car name: Blackfoot
Car Type: Remodel
Top speed: 58 kph
Rating/Class: 2 (Advanced)
Installed folder: ...\cars\blackfoot86
Description: 

This is the first car I've ever made for Re-Volt.

It's a Tamiya Blackfoot, which was originally released in 1986. In 2016, the Monster Truck was re-released,
but some details was changed (Especially the Ford brand, and some other details). I've decided to do the
86-version, so you can find Ford-logos and the 1980 Ford Ranger grill.

I took the stock Monster Truck "BigVolt" as a basis and adjusted some details, like a new front-bumper, new
wheels, circle headlights, added shocks, and so on. I also modified the car parameters, so it's now a class
higher. I tried to adjust the parameters, that the characteristics of the Original Blackfoot can be found
(you only have to search deep engogh, hehe) but I also wanted the car good to drive in races (what is not the
intention of the original model for sure). I also tried to make the car not to easy to drive, so it has his
unique character, and driving get not boring after some races.

The car has a rear wheel drive, which make it understeer sometimes and a bit hard to handle in some situations. 
But after some laps of learning, it's fun to drive with it, and it can win races against cars in the same
class like Panga TC or R6 Turbo.

Author Information
================================================================
Author Name: Kiwi
Email Address: ingenieur.kiwi@gmail.com
Other Info: I own Re-Volt since 1999. I'm a casual driver, and not the best in it. I race just for fun, and
Re-Volt is the perfect game for this. I'm new to modelling - for Blackfoot I worked the first time with a 3D
modelling software. But I have some texturing knowledge - I made skins for the game TOCA2 und TOCA Race Driver
in the beginning of the 2000s.
 
Construction
================================================================
Base: BigVolt (Body) from Acclaim, APC-L13 (Wheels) from Allan1
Editor(s) used: Notepad++, Blender (with HabitatB-Plugin), PhotoImpact12
 
Additional Credits 
================================================================
Thanks to the guys at RVIO community in Discord (#creations) for your help
Javes for his beta testing and thoughts
URV for his test drives, thoughts and hints for car parameters
Gotolei for his BigVolt Remap (Which i didn't used in the end, but helped me in finding out how mapping works)

Copyright / Permissions
================================================================
You may do whatever you want with this car, as long as you mention me in the credits.

HAVE FUN!

Kiwi