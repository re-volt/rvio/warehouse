Car Information
================================================================
Car name : Sand Runner
Car Type : Remodel
Top Speed : 36mph / 58kph
Rating/Class : 3 (semi-pro)
Install folder : ...\cars\sandrunner
Description: Remodel of BurnRubr's conversion "BUGGY"

Edited for much cleaner mapping, geometry and about a third of the polies.
Has actual axles/springs instead of static models on the body.

Also actually handles like a buggy instead of a Toyeca repaint.
Having a lot of grip for its weight and very large wheels, it can be a handful to handle.
Batteries are a bit of a wild card, often working better off-road than on-road.


Author Information
================================================================
Author Name : Raster Gotolei
Email Address : rgotolei@gmail.com


Construction
================================================================
Base : BUGGY
Editors used : Blender for modeling, gimp for texturing


Additional Credits
================================================================
Original modelers of 4x4 Offroad Adventure
BurnRubr for conversion
Jigebren for blender plugin and prm2hul


Copyright / Permissions
================================================================
You may do whatever you want with this car.
XCFs and mapping templates in RVL thread download.
