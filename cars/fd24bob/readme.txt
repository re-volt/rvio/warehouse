﻿Car information
================================================================
Car name                : Mr. Bob
Car Type  		: Original
Top speed 		: 36.1 mph
Rating/Class   		: Semi-pro
Installed folder       	: ...\cars\fd24bob

Author Information
================================================================
Author Name 		: Xarc
Email Address           : warrockrockwar@hotmail.it
 
Construction
================================================================
Base           		: Original
Editor(s) used 		: Blender and MediBang Paint Pro
 
Additional Credits 
================================================================
Credits to jigebren for the Blender plugin and prm2hul

Copyright / Permissions
================================================================
You may do whatever you want with this CAR.