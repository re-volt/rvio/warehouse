VERSION 1.2:
-Changed the parameters to something more interesting
-Made some changes to the texture
-Added preview image

VERSION 1.1:
-Fixed the wheel radius
-Removed some pureblacks from the carbox

==============================================================================================================

Thanks for trying out my car! You can leave any feedback or suggestions at its page on revoltzone.net or creations.re-volt.io/.

CREDITS:
- Models and texture by Wichilie.
- Parameters by Wichilie and Acclaim (AI section taken from Humma).

TOOLS:
Made using Photoshop and Blender with Jigebren's and Marv's plugins.

PERMISSIONS:
You are free to distribute these files anywhere as long as:
- You make it available for free.
- You leave this ReadMe intact.

The texture and parameters are free to be edited and used as long as:
- You make it available for free.
- You mention me (Wichilie) in your ReadMe as the original author.