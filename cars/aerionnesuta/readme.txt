Car information
================================================================
Car name                : Rionnesta
Car Type  		: Original
Top speed 		: 68 kph
Rating/Class   		: 4 (pro)
Installed folder       	: ...\cars\aerionnesuta
Description             : an unique concept never seen on any aero car before

Rionnesta is an aero car based off Tamiya's Mini 4WD "Diospada". 
unlike every other cars, this car uses the rear wheels to steer. if that is not unique enough, the whole rear wing moves in accordance to the steering, making it a "rear wing steering" driven car.
the car has sleek super car design combined with F1 aerodynamic designs. the front of the car is of a traditional super car, the rest is a smoothly stylized aerodynamic body.
mainly colored in orange, it has black line vinyls with white outlines along with carbon fiber markings around the cockpit.

Rionnesta's performance highlights its unique characteristics of a rear steer drive. it has a controllable understeery handling which emphasizes its rear steering.
the car has a relatively high speed among pro class cars, beating Toyeca by a small fraction. its handling is quite loose with tight understeer.
due to the unique steering, the car can easily drive across ice surfaces without spinning out in cost of heavy understeer on ice.
the acceleration is decently average, which combined with its loose and understeery handling, may prove the car to be sluggish.

overall, Rionnesta's unique innovative concept of "rear wing based" rear steering will appeal to racers and creators alike.


Author Information
================================================================
Author Name 		: shara coronvi
Email Address           : sharacolonvee@mail.com
Other Info		: pretty much a recent Re-Volt veteran. 
                          a good racer and drifter. experienced in parametering and repaints.
                          creator of aerodynamic car designs. also a drawing "artist" too.
 
Construction
================================================================
Base           		: Original
Editor(s) used 		: Notepad, Paint.NET, SketchUp and Blender
 
Additional Credits 
================================================================
Thanks and Credits to those who inspired or helped.

Acclaim for this great game
Microsoft for Notepad
Paint.NET developers for the afromentioned program
Marv for the Blender plugin
Shou (SKarma) for telling how SHADOWTABLE values work
Mladen for the .hul collision file and for the fixes on the "rear wheel rotation" update.


Copyright / Permissions
================================================================
You may do whatever you want with this CAR.

however, if you're considering to put this car in a pack, you may NOT modify the car's handling.
if the car handling doesnt fit into the theme of the pack (ie too slow, too fast), simply dont put this car in.
though, you are ALLOWED to make a NEW car using this as a base, and then put that new car into a pack.
