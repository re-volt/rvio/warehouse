{

;============================================================
;============================================================
; Nice Black
;============================================================
;============================================================
Name       	"Nice Black"


;====================
; Stuff mainly for frontend display and car selectability
;====================

BestTime   	true
Selectable 	true
;)CPUSelectable	true
;)Statistics 	true
Class      	1 			; Engine type (0=Elec, 1=Glow, 2=Other)
Obtain     	0 			; Obtain method
Rating     	3 			; Skill level (rookie, amateur, ...)
TopEnd     	3204.003906 			; Actual top speed (mph) for frontend bars
Acc        	5.447958 			; Acceleration rating (empirical)
Weight     	0.950000 			; Scaled weight (for frontend bars)
Trans      	0 			; Transmission type (0=4WD, 1=FWD, 2=RWD)
MaxRevs    	0.700000 			; Max Revs (for rev counter, deprecated...)
;)TCARBOX  	"cars/niceblack/carbox.bmp" 		; Carbox texture

;====================
; Model Filenames
;====================

MODEL 	0 	"cars/niceblack/body.prm"
MODEL 	1 	"cars/niceblack/wheelfl.prm"
MODEL 	2 	"cars/niceblack/wheelfr.prm"
MODEL 	3 	"cars/niceblack/wheelbl.prm"
MODEL 	4 	"cars/niceblack/wheelbr.prm"
MODEL 	5 	"NONE"
MODEL 	6 	"NONE"
MODEL 	7 	"NONE"
MODEL 	8 	"NONE"
MODEL 	9 	"NONE"
MODEL 	10 	"NONE"
MODEL 	11 	"NONE"
MODEL 	12 	"NONE"
MODEL 	13 	"NONE"
MODEL 	14 	"NONE"
MODEL 	15 	"NONE"
MODEL 	16 	"NONE"
MODEL 	17 	"cars/misc/Aerial.m"
MODEL 	18 	"cars/misc/AerialT.m"
COLL 	"cars/niceblack/hull.hul"
TPAGE 	"cars/niceblack/car.bmp"
;)TSHADOW 	"cars/niceblack/shadow.bmp"
;)SHADOWINDEX 	-1
;)SHADOWTABLE -48.8 48.8 77.2 -81.2 -7.4 	; Left, right, front, back, height (relative to model center)
EnvRGB 	200 200 200

;====================
; Handling related stuff
;====================

SteerRate  	2.000000 			; Rate at which steer angle approaches value from input
SteerMod   	0.400000 			;
EngineRate 	5.000000 			; Rate at which Engine voltage approaches set value
TopSpeed   	35.799999 			; Car's theoretical top speed (not including friction...)
DownForceMod	2.000000 			; Down force modifier when car on floor
CoM        	0.000000 2.000000 -2.000000 		; Centre of mass relative to model centre
Weapon     	0.000000 -16.000000 32.000000 		; Weapon generation offset
;)Flippable	false 			; Rotor car effect
;)Flying   	false 			; Flying like the UFO car
;)ClothFx  	false 			; Mystery car cloth effect

;====================
; Car Body details
;====================

BODY {		; Start Body
ModelNum   	0 			; Model Number in above list
Offset     	0.000000 0.000000 0.000000
Mass       	1.000000
Inertia    	1344.000000 0.000000 0.000000
           	0.000000 1550.000000 0.000000
           	0.000000 0.000000 365.000000
Gravity    	2200 			; No longer used
Hardness   	0.000000
Resistance 	0.001000 			; Linear air resistance
AngRes     	0.001000 			; Angular air resistance
ResMod     	23.000000 			; AngRes scale when in air
Grip       	0.030000 			; Converts downforce to friction value
StaticFriction 0.800000
KineticFriction 1.400000
}     		; End Body

;====================
; Car Wheel details
;====================

WHEEL 0 { 	; Start Wheel
ModelNum 	1
Offset1  	-22.500000 -3.000000 32.000000
Offset2  	-2.000000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	true
SteerRatio  	-0.400000
EngineRatio 	9500.000000
Radius      	11.000000
Mass        	0.120000
Gravity     	2200.000000
MaxPos      	3.000000
SkidWidth   	14.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    	0.020000
Grip            	0.018000
StaticFriction  	1.550000
KineticFriction 	2.350000
}          	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum 	2
Offset1  	22.500000 -3.000000 32.000000
Offset2  	2.000000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	true
SteerRatio  	-0.400000
EngineRatio 	9500.000000
Radius      	11.000000
Mass        	0.120000
Gravity     	2200.000000
MaxPos      	3.000000
SkidWidth   	14.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    	0.020000
Grip            	0.018000
StaticFriction  	1.550000
KineticFriction 	2.350000
}          	; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum 	3
Offset1  	-24.000000 -2.500000 -40.000000
Offset2  	-2.000000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	false
SteerRatio  	0.100000
EngineRatio 	8500.000000
Radius      	11.000000
Mass        	0.120000
Gravity     	2200.000000
MaxPos      	3.000000
SkidWidth   	18.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    	0.050000
Grip            	0.018000
StaticFriction  	1.550000
KineticFriction 	3.350000
}          	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum 	4
Offset1  	24.000000 -2.500000 -40.000000
Offset2  	2.000000 0.000000 0.000000
IsPresent   	true
IsPowered   	true
IsTurnable  	false
SteerRatio  	0.100000
EngineRatio 	8500.000000
Radius      	11.000000
Mass        	0.120000
Gravity     	2200.000000
MaxPos      	3.000000
SkidWidth   	18.000000
ToeIn       	0.000000
;)Camber    	0.000000
AxleFriction    	0.050000
Grip            	0.018000
StaticFriction  	1.550000
KineticFriction 	3.350000
}          	; End Wheel


;====================
; Car Spring details
;====================

SPRING 0 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	1000.000000
Damping     	15.000000
Restitution 	-0.850000
}           	; End Spring

SPRING 1 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	1000.000000
Damping     	15.000000
Restitution 	-0.850000
}           	; End Spring

SPRING 2 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	800.000000
Damping     	12.000000
Restitution 	-0.850000
}           	; End Spring

SPRING 3 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	800.000000
Damping     	12.000000
Restitution 	-0.850000
}           	; End Spring


;====================
; Car Pin details
;====================

PIN 0 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 1 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 2 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 3 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin


;====================
; Car axle details
;====================

AXLE 0 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 1 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 2 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 3 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle


;====================
; Car spinner details
;====================

SPINNER {   	; Start spinner
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Axis        	0.000000 1.000000 0.000000
AngVel      	0.000000
;)Type      	1 			; 1: Default rot, 2: Turn with steer, 4: Translate with speed, 6: 2 and 4
;)Trans     	0.000000 3.000000 6.000000 		; Translation max
;)TransVel  	0.001000 			; Velocity factor
}           	; End Spinner


;====================
; Car Aerial details
;====================

AERIAL {    	; Start Aerial
SecModelNum 	17
TopModelNum 	18
Offset      	0.000000 -20.000000 -45.000000
Direction   	0.000000 -1.000000 0.000000
Length      	25.000000
Stiffness   	1000.000000
Damping     	1.500000
}           	; End Aerial


;====================
; Car AI details
;====================

AI {        	 ;Start AI
UnderThresh 	207.720001
UnderRange  	1500.000000
UnderFront  	450.000000
UnderRear   	386.410004
UnderMax    	0.950000
OverThresh  	100.000000
OverRange   	1403.790039
OverMax     	0.350000
OverAccThresh  	154.380005
OverAccRange   	611.049988
PickupBias     	6553
BlockBias      	6553
OvertakeBias   	22936
Suspension     	0
Aggression     	0
}           	; End AI

}

