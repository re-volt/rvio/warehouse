Car information
================================================================
Car name : Radar Ranger
Car Type : Original
Top speed : 35 mph / 57 kph
Rating/Class : 2 (advanced)
Installed folder : .../cars/radar-ranger
Description : My first completed original car. Based on the Hot Wheels model of the same name. 

Params are mostly based on Bertha, but tops off a bit higher.

Irregular shape and low CoM means it's pretty much impossible to flip this thing over, but don't expect it to be perfectly stable either.


Author Information
================================================================
Author Name : Raster Gotolei
Email Address : rgotolei@gmail.com


Construction
================================================================
Source : Radar Ranger Hot Wheels model
Editor(s) used : blender for modeling/mapping, gimp for skin/carbox, inkscape for wheels, pluma for text stuff


Additional Credits
================================================================
Larry Wood for designing the original vehicle
Jigebren for Blender plugin


Copyright / Permissions
================================================================
You may do whatever you want with this car, just have some courtesy you know?
XCFs/SVGs in RVL thread download.
