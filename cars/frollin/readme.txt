﻿Car information
================================================================
Car name                : Frollin
Car Type  		: Original
Top speed 		: 34 mph
Rating/Class   		: Advanced
Installed folder       	: ...\cars\fd1frollin

Author Information
================================================================
Author Name 		: Xarc
Email Address           : warrockrockwar@hotmail.it
 
Construction
================================================================
Base           		: Original
Editor(s) used 		: Blender and MediBang Paint Pro
 
Additional Credits 
================================================================
Credits to jigebren for the Blender plugin and prm2hul

Copyright / Permissions
================================================================
You may do whatever you want with this CAR.