Car information
================================================================
Car name: SuperBoss
Car Type: Remodel
Top speed: 68 kph
Rating/Class: 4 (Pros)
Installed folder: ...\cars\superboss
Description: 

When BossVolt is a ship, then SuperBoss is a battleship!

Ultra heavy, extreme top speed, bad acceleration.

Comes with an alternative livery called "Bandag Bandit".

The model which inspired me to do SuperBoss, is a Matchbox car from 1982.

Have fun! :)

Kiwi (Remodel, Textures), R6te (Parameters)


Author Information
================================================================
Remodel: Kiwi
Textures: Kiwi
Parameters: R6te
Base model: BossVolt by Acclaim
 
Construction
================================================================
Base: BossVolt by Acclaim
Editor(s) used: Notepad++, Blender (with ReVolt-Plugin from Marv), PhotoImpact12
 
Additional Credits 
================================================================
+ Thanks to the guys at the RVIO community in Discord (#cars) for your help and suggestions!


Copyright / Permissions
================================================================
You may do whatever you want with this car, as long as you mention us and the original author in the credits.

HAVE FUN!

Kiwi & R6te

Version 1.1 from November 28th, 2018