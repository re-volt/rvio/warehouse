August 14, 2017: Readme file of Pix Skull


================================================================
======================== Car Info ==============================
================================================================

Name			Pix Skull
Author			Allan1
Date			September 15, 2013
Type			Original
Folder Name		pixel_skull
Rating			Pro
Class			Glow
Drivetrain		Four-wheel Drive
Top speed		41 mph
Body mass		1.4 Kg


================================================================
========================= Setup ================================
================================================================

Extract the zipped file into your Re-Volt directory. Simple as that.



================================================================
======================= Description ============================
================================================================

That's a monster truck similar to Mad Max's style of vehicles: Dirty and punk. It's quite fast and can race well beside the stock pros. Dreamcast tho... Well, it gives you more challenge.



================================================================
======================== Credits ===============================
================================================================

Skull logo comes from the Hot Wheels' car Way 2 Fast.

Track in the preview picture is Offroad Desert by R6te and Miro.

Tools:
Autodesk 3D Studio Max 2009
Asetools (By Ali)
Re-Volt 1.2 (By Huki and Jig)
CorelDRAW x3
Prm2Hul (By Jig)
Adobe Photoshop CS2
MS Notepad


================================================================
======================== Changelog =============================
================================================================

November 06, 2015:
-New models of springs
-Increased speed and acceleration
-Fixed carbox

August 14, 2017:
-Fixed center of mass and body AngRes
-Fixed axles and springs
-Minor changes in parameters


================================================================
=================== Permissions and terms ======================
================================================================

(This creation don't have a license. Even so, please be polite and respect the author wishes)

You're free to share (copy and redistribute the material in any medium or format) and adapt (remix, transform, and build upon the material) this creation, provided that:

1. You give appropriate credit and indicate if changes were made. You may do so in any reasonable manner.

2. You'll not use the material for commercial purposes.

3. If you remix, transform, or build upon the material, you must distribute your contributions under the same terms/permissions as the original.

4. You may not apply legal terms or technological measures that legally restrict others from doing anything the author permits.


================================================================
======================= Final notes ============================
================================================================

Please contact me if you find bugs or if you have any suggestion or idea. You can do so at the comments section of Re-Volt Zone or Dropbox, or even with a private message at the forum Re-Volt Live or via email.

My email adress: allanmoraes27@yahoo.com

Re-Volt Live profile: http://z3.invisionfree.com/Revolt_Live/index.php?s=90eae8a917757853e685db4ff40ec0a2&showuser=486



For more, visit my website: http://allanhqs.wix.com/warehouse


Have a nice Race!

Allan