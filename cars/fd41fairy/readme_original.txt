================================================================
Car name                : Lamborghini Countach
Install in folder       : Re-volt\cars\LamCountach
Author                  : BurnRubr/Nairb/RiffRaff
Email Address           : burnrubr@xoommail.com
Misc. Author Info	: Web Site = http://members.xoom.com/BurnRubr/

Details			: This car comes with a lot of extra skins
			  (1 by Barney, 5 by Nairb, and 2 by Nairb's
			  brother Jason).  To use another skin, just
			  rename your choice to "Lambo.bmp" and rename
			  the default to Nairb1.bmp.  Big thanks go
			  out to RiffRaff for making the cool custom
			  wheels, they add a lot to make this car look
			  even cooler.
================================================================

* Play Information *

Top speed (observed)    : 44 mph
Rating                  : Pro

* Construction *

Base                    : Body converted from "Need For Speed" and
			  custom wheels created by RiffRaff
Editor(s) used          : 3ds MAX, Photoshop


* Copyright / Permissions *

Authors MAY use this *Car* as a base to build additional cars.

You MAY distribute this CAR, provided you include this file, with
no modifications.  You may distribute this file in any electronic
format (BBS, Diskette, CD, etc) as long as you include this file 
intact.