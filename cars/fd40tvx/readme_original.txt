Car information
================================================================
Car name                : Night Drifter
Car Type  		: Original
Top speed 		: 47.mph
Rating/Class   		: 4 (pro)
Installed folder       	: ...\cars\nightdrifter
Description             : This is a car loosely moddeled off of the Hot wheels car Mid Drift.
			: Also, first car, if you wanted to know.
 
Author Information
================================================================
Author Name 		: BBLIR
 
Construction
================================================================
Editor(s) used 		: Blender, Photoshop, MS Paint
 
Copyright / Permissions
================================================================
Nothing special, just have fun and don't claim it as your own.