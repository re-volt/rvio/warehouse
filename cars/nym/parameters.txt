{

;============================================================
;============================================================
; Nymrod
;============================================================
;============================================================
Name      	"Nymrod"


;====================
; Model Filenames
;====================

MODEL 	0 	"cars\nym\body.prm"
MODEL 	1 	"cars\nym\wheelf-l.prm"
MODEL 	2 	"cars\nym\wheelf-r.prm"
MODEL 	3 	"cars\nym\wheelb-l.prm"
MODEL 	4 	"cars\nym\wheelb-r.prm"
MODEL 	5 	"NONE"
MODEL 	6 	"NONE"
MODEL 	7 	"NONE"
MODEL 	8 	"NONE"
MODEL 	9 	"NONE"
MODEL 	10 	"NONE"
MODEL 	11 	"NONE"
MODEL 	12 	"NONE"
MODEL 	13 	"NONE"
MODEL 	14 	"NONE"
MODEL 	15 	"cars\nym\1.m"
MODEL 	16 	"cars\nym\2.m"
MODEL 	17 	"NONE"
MODEL 	18 	"NONE"
TPAGE 	"cars\nym\car.bmp"
;)TCARBOX  	"cars/nym/carbox.bmp" 			
;)TSHADOW 	"cars/nym/shadow.bmp"
;)SHADOWINDEX 	-1 			
;)SHADOWTABLE 	-51.6 51.6 74.5 -90.2 -5.9
;)SFXENGINE 	"cars\nym\electric.wav"
;)SFXSERVO 	"NONE"
;)SFXHONK 	"NONE"
COLL 	"cars\nym\hull.hul"
EnvRGB 	200 200 200

;====================
; Stuff mainly for frontend display and car selectability
;====================

;)CPUSelectable	true
;)Statistics	TRUE
BestTime   	TRUE
Selectable 	TRUE
Class      	0 			; Engine type (0=Elec, 1=Glow, 2=Other)
Obtain     	0 			; Obtain method
Rating     	5 			; Skill level (rookie, amateur, ...)
TopEnd     	39500.879639 			; Actual top speed (mph) for frontend bars
Acc        	5.999665 			; Acceleration rating (empirical)
Weight     	1.450000 			; Scaled weight (for frontend bars)
Handling   	50.000000 			; Handling ability (empirical and totally subjective)
Trans      	2 			; Transmission type (calculate in game anyway...)
MaxRevs    	0.500000 			; Max Revs (for rev counter)

;====================
; Handling related stuff
;====================

SteerRate  	3.000000 			; Rate at which steer angle approaches value from input
SteerMod   	0.400000 			;
EngineRate 	7.0000000 			; Rate at which Engine voltage approaches set value
TopSpeed   	44.850000 			; Car's theoretical top speed (not including friction...)
DownForceMod	2.000000 			; Down force modifier when car on floor
CoM        	0.000000 0.120000 6.000000 		; Centre of mass relative to model centre
Weapon     	0.000000 -32.000000 64.000000 		; Weapon genration offset

;====================
; Car Body details
;====================

BODY {		; Start Body
ModelNum   	0 			; Model Number in above list
Offset     	0, 0, 0 		; Calculated in game
Mass       	1.450000
Inertia    	1500.000000 0.000000 0.000000
           	0.000000 2000.000000 0.000000
           	0.000000 0.000000 800.000000
Gravity		2200 			; No longer used
Hardness   	0.000000
Resistance 	0.001000 			; Linear air esistance
AngRes     	0.011000 			; Angular air resistance
ResMod     	25.000000 			; Ang air resistnce scale when in air
Grip       	0.010000 			; Converts downforce to friction value
StaticFriction 0.790000
KineticFriction 0.430000
}     		; End Body

;====================
; Car Wheel details
;====================

WHEEL 0 { 	; Start Wheel
ModelNum 	1
Offset1  	-25.500000 -4.000000 31.000000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	FALSE
IsTurnable  	TRUE
SteerRatio  	-0.350000
EngineRatio 	5000.000000
Radius      	11.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	3.000000
SkidWidth   	14.000000
ToeIn       	0.000000
AxleFriction    	0.020000
Grip            	0.015000
StaticFriction  	1.900000
KineticFriction 	1.900000
}          	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum 	2
Offset1  	25.500000 -4.000000 31.000000
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	FALSE
IsTurnable  	TRUE
SteerRatio  	-0.350000
EngineRatio 	5000.000000
Radius      	11.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	3.000000
SkidWidth   	14.000000
ToeIn       	0.000000
AxleFriction    	0.020000
Grip            	0.015000
StaticFriction  	1.900000
KineticFriction 	1.900000
}          	; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum 	3
Offset1  	-27.000000 -7.500000 -56.500000
Offset2  	0.000000 0.000000 0.00000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	FALSE
SteerRatio  	0.100000
EngineRatio 	40000.000000
Radius      	14.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	3.000000
SkidWidth   	14.000000
ToeIn       	0.000000
AxleFriction    	0.030000
Grip            	0.025000
StaticFriction  	2.100000
KineticFriction 	2.100000
}          	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum 	4
Offset1  	27.000000 -7.500000 -56.500000
Offset2  	0.000000 0.000000 0.00000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	FALSE
SteerRatio  	0.100000
EngineRatio 	40000.000000
Radius      	14.000000
Mass        	0.150000
Gravity     	2200.000000
MaxPos      	3.000000
SkidWidth   	14.000000
ToeIn       	0.000000
AxleFriction    	0.030000
Grip            	0.025000
StaticFriction  	2.100000
KineticFriction 	2.100000
}          	; End Wheel


;====================
; Car Spring details
;====================

SPRING 0 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	600.000000
Damping     	10.000000
Restitution 	-0.850000
}           	; End Spring

SPRING 1 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	600.000000
Damping     	10.000000
Restitution 	-0.850000
}           	; End Spring

SPRING 2 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	600.000000
Damping     	10.000000
Restitution 	-0.850000
}           	; End Spring

SPRING 3 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	600.000000
Damping     	10.000000
Restitution 	-0.850000
}           	; End Spring


;====================
; Car Pin details
;====================

PIN 0 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 1 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 2 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 3 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin


;====================
; Car axle details
;====================

AXLE 0 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 1 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 2 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 3 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle


;====================
; Car spinner details
;====================

SPINNER {   	; Start spinner
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Axis        	0.000000 1.000000 0.000000
AngVel      	0.000000
}           	; End Spinner


;====================
; Car Aerial details
;====================

AERIAL {    	; Start Aerial
SecModelNum 	15
TopModelNum 	16
Offset      	20.000000 -17.000000 21.000000
Direction   	0.000000 -1.000000 0.000000
Length      	17.000000
Stiffness   	2000.000000
Damping     	2.500000
}           	; End Aerial


;====================
; Car AI details
;====================

AI {        	 ;Start AI
UnderThresh 	150.000000
UnderRange  	1500.000000
UnderFront	 	611.510010
UnderRear   	682.195496
UnderMax    	0.950000
OverThresh  	2003.814819
OverRange   	1391.000000
OverMax     	0.316012
OverAccThresh  	145.010406
OverAccRange   	2474.104980
PickupBias     	16383
BlockBias      	16383
OvertakeBias   	16383
Suspension     	9830
Aggression     	0
}           	; End AI

}

