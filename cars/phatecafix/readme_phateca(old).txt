================================== Phateca by Kriss =============================
Name: Phateca
Author: Kriss
Rank: Rookie
Models used: Toyeca's body, Phat Slug's Body, Toyeca's wheels, Phat Slug's springs and axles.
Programs used: Zmodeler 1.07b, rvshade.exe, Re-volt's MAKEITGOOD, RVGlue.exe, rvtexmap.exe,
	        Photoshop CS4, Paint, Notepad.

Full installer: It is made with Notepad by Kriss - me. To install correctly you should put "install_phateca.zip"
	    file from Cars\phateca to your Re-volt directory (eg. C:\Program Files\Acclaim Entertainment\
	    Re-volt) and click "Extract here". Double click on "install_phateca.bat". When you will install
	    this car than at Sprinter XL box's place (in Re-volt) will be Phateca. To uninstall run
	    "Uninstall_phateca.bat" at your Re-volt directory.

More: This car is made for Phat Slug MOD on Re-volt LIVE Forum. Thanks for LeetCake because he was
         creating Phat Slug MOD and I think of idea - crossing of Toyeca and Phat Slug.

				Toyeca + Phat Slug = Phateca

							Thanks for downloading!
						Kriss, Latvia 27th February 2010 year.

(
little addition by Kipy:
you can get the car here:
http://revoltzone.net/cars/2038/Phateca
)