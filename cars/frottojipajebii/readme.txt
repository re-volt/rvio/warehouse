Car information
================================================================
Car name                : Proto Zipper JB
Car Type  		: Remodel
Top speed 		: 62 kph (45 kph Average)
Rating/Class   		: 3 (semi-pro)
Installed folder       	: ...\cars\frottojipajebii
Description             : 

this is my first remodel i did

Proto Zipper JB, as the name says, is a remodel of Zipper. the car takes motifs from Tamiya's Mini 4WD "Proto Saber JB".
the car is redesigned to have a more aerodynamical body and more offroad tolerance. the result is a new design that puts forth as Zipper's aerodynamical counterpart.
Proto Zipper JB is mainly colored in blue with red and black streaks along with yellow accents that gives the car a slick visual look.

the car is balanced against Semi-Pro cars. it has a high top speed for its class with mediocre acceleration. the car highlights is its handling style and stability.
this car's handling is floaty and loose. it turns with a slide. maintaining speed with this handling style is difficult while making agile moves is easier.
the biggest advantage of this car is its stability. it can take on any surface imaginable with effortless ease. bumps are nothing for this car as the car still has full control when driving over uneven terrains.
ice roads isnt a problem either. the handling style makes sliding across slippery terrains easier. hard landings is nothing to worry about, landings have no impacts whatsoever.
not only that, the car also has the ability to scale across walls on a short period. this wont be helpful during normal situations, but on some tracks, its possible to take shortcuts making use of this ability.

Proto Zipper JB is a formidable car for its stability and unusual handling style

Author Information
================================================================
Author Name 		: shara coronvi
Email Address           : sharacolonvee@mail.com
Other Info		: pretty much a recent Re-Volt veteran. 
                          a good racer and drifter. experienced in parametering and repaints. newcomer to modelling. 
 
Construction
================================================================
Base           		: tc1
Editor(s) used 		: Notepad, Paint.NET, SketchUp and Blender
 
Additional Credits 
================================================================
Thanks and Credits to those who inspired or helped.

Acclaim for this great game
Microsoft for Notepad
Paint.NET developers for the afromentioned program
Mladen for the shadow and the collision hull
Marv for the Blender plugin



Copyright / Permissions
================================================================
You may do whatever you want with this CAR.

however, if you're considering to put this car in a pack, you may NOT modify the car's handling.
if the car handling doesnt fit into the theme of the pack (ie too slow, too fast), simply dont put this car in.
though, you are ALLOWED to make a NEW car using this as a base, and then put that new car into a pack.
 
