﻿Car information
================================================================
Car name                : Sater XL
Car Type  		: Original
Top speed 		: 42.0 mph
Rating/Class   		: Professional
Installed folder       	: ...\cars\fd22sater

Author Information
================================================================
Author Name 		: Xarc
Email Address           : warrockrockwar@hotmail.it
 
Construction
================================================================
Base           		: Original
Editor(s) used 		: Blender and MediBang Paint Pro
 
Additional Credits 
================================================================
Credits to jigebren for the Blender plugin and prm2hul

Copyright / Permissions
================================================================
You may do whatever you want with this CAR.