﻿Car information
================================================================
Car name                : F2 Flamma
Car Type  		: Original
Top speed 		: 35.9 mph
Rating/Class   		: Advanced
Installed folder       	: ...\cars\fd28f2

Author Information
================================================================
Author Name 		: Xarc
Email Address           : warrockrockwar@hotmail.it
 
Construction
================================================================
Base           		: Original
Editor(s) used 		: Blender and MediBang Paint Pro
 
Additional Credits 
================================================================
Credits to jigebren for the Blender plugin and prm2hul

Copyright / Permissions
================================================================
You may do whatever you want with this CAR.