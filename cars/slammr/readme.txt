=====================================================================
=====================================================================
=====================================================================
=====================================================================
Details:

Name:                           Nano Slammr
Authors:                    Kipy & burner94
Category:                          Original
Class:                             Advanced    
Engine type:                       Electric
Trans:                                  AWD
Top Speed:             36.0 mph / 57.9 km/h
Folder's name:       .../ReVolt/cars/slammr/

=====================================================================
=====================================================================
=====================================================================
=====================================================================
=====================================================================

Tools:

Blender 2.76 + jig's plugin
Paint.NET
autoShade CarLighting
prm2hul

=====================================================================
=====================================================================
Credits:

Nano Slammr is a car built by Nikko R/C and the one what this car
based on. Texture is slightly different, not the exact replica.
Fun fact: this car model is 100% scratch-made this time as well.

Special thanks goes to burner who made this (FIRST!) 5-wheeler
to work.

Copyright belongs to Kipy who made the car itself.
You can repaint/modify, but ALWAYS mention the authors of the car.

=====================================================================
=====================================================================
							2018.06.24.
=====================================================================
=====================================================================