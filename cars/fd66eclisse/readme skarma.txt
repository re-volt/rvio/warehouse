﻿10:05 02/11/2013

	~ ~ ~ ~ ~ ~ ~ ~ ~ ~ SKARMINATER　プレゼント ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

	     	        <Mitsubishi Eclipse GSX>

＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
（Preface)

Been a long time since I last released a Mitsubishi.
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
（Car Information)

Date	  : 25/03/2013
Car Name  : Mitsubishi Eclipse GSX
Car Type  : Conversion
Folder    : ...\cars\egsx
Install   : As with any custom car, just drag the cars folder into your Re-Volt folder.
Top Speed : 39.1 MPH (observed)
Rating    : Advanced
Car #     : 435
Release # : 169

Uses Cat's wheel mesh. Yet another car I'm revisiting. Originally converted by Scloink many many years ago, this is my version. Stock, handles great, pretty fun car.
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
（Construction)

Base           : Original NFS4 scratch model by Ryuji Kainoh
Editor(s) used : NFS Wizard, Zanoza Modeler 1.07a, Car:Load, PRM2HULL, Adobe Photoshop, Paint.net, Notepad
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
（Thanks)

Ryuji Kainoh for the original model.
Cat for his wheel mesh.
AGT for the Blizzard Beach track where I took the picture.
hilaire9 for his huge selection of tracks in general.
Zach/ZAgames for hosting RVZ.
KDL for the Car:Load and CarLighter tools.
Jigebren for his PRM2HULL tool.
ZR, Nero, UR, Marv, Mezza, Mmud & the gentlemen in #ReVolt-Chat. Because I say so.
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
(Copyrights)

You may do whatever you like with this car providing you keep it in a presentable and respectable manner.

However, I will not appreciate seeing this car scaled back to 1207 standards unless it is for personal use. There is no reason for anyone to do this now anyway seeing as I'm not going to be stingy anymore. But still... Please don't do it.
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
(Where to find this car)

Skarma's Derelict Corner - My own website where you can find all of my cars and more.

This car will also be uploaded to RVZ. It may also be found on other sites such as RVA, ARM and Last_Cuban's XTG website.

This car may be uploaded to any of the above sites without consent. PLEASE ask me if you would like to upload it anywhere else.

Visit my WIP thread at RVL to see what other cars I have going on! Link underneath.

＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿

http://z3.invisionfree.com/Revolt_Live/index.php?showtopic=1595
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿

Visit my website, Skarma's Derelict Corner at http://skarma.cz.cc or http://bit.ly/skarma
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
（Disclaimer)

By downloading this car, you are using it at your own risk & discretion. I am not responsible for ANY problems that this car may cause to your computer.

This car SHOULD still work for you no matter what version you're using (bar 1.0). I still recommend that you at least have the 1102 version of the 1.2 patch to fully enjoy this car the way it was intended to be.
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
Have fun, etc...