Mitsubishi Eclipse GSX for Need for speed IV

Title          : Mitsubishi Eclipse GSX
Car            : Mitsubishi Eclipse GSX `98 (SN:29)
File           : egsx.zip
Version        : 1.0 (Upgrade Feature : NO)
Date           : APR 2001

Author         : Ryuji KAINOH
Email          : ryuji_k@iris.eonet.ne.jp
Homepage       : http://rkncs.totalnfs.net/

Used Editor(s) : Mrc(cartool.zip) by EA
               : NFS Wizard v0.5.0.79 by Jesper Juul-Mortensen
               : VIV Wizard v0.8(build 297) by Jesper Juul-Mortensen
               : VIV Wizard v0.8(build 299) by Jesper Juul-Mortensen
               : NFSIII Car CAD v1.4b by Chris Barnard
               : NFS Car CAD v1.5b by Chris Barnard
               : CAR3TO4 by J�Eg Billeter
               : NFS FCEConverter by Addict&Rocket
               : PaintShop Pro 5J
Thanks.
___________________________________________________________

Installation : Put the "car.viv" in "Data\Cars\egsx".
               and  the "egsx.qfs" in "Data\FeArt\VidWall".

Have a fun !!