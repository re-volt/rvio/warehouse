November 06, 2015: Readme file of Jaguar


================================================================
======================== Car Info ==============================
================================================================

Name			Jaguar
Author			Allan1
Date			October 22, 2013
Type			Original
Folder Name		leo
Rating			Amateur
Class			Electric
Drivetrain		Four-wheel Drive
Top speed		33 mph
Body mass		1.1 kg


================================================================
========================= Setup ================================
================================================================

Extract the zipped file into your Re-Volt directory. Simple as that.


================================================================
======================= Description ============================
================================================================

A car based on Jaguar XKE. It was a British sport car manufactured by 'Jaguar Cars Ltd' between 1961 and 1975.
I made this car for nostalgic reasons. Do you know a game called Interstate '76? Well, its expansion pack (the Nitro Pack) contains this car, and it was one of my favorites in the game. The mesh and paintjob style were borrowed from it, as well as real Jaguars around.


================================================================
======================== Credits ===============================
================================================================

Credits for the base design comes to Jaguar Cars Ltd and Interstate '76 Nitro Pack.

Track in the picture is Toon Street by Skitch2.

Tools:
Autodesk 3D Studio Max 2009
Asetools (By Ali)
Re-Volt 1.2 (By Huki and Jig)
CorelDRAW x3
Prm2Hul (By Jig)
Adobe Photoshop CS2
MS Notepad


================================================================
======================== Changelog =============================
================================================================

November 06, 2015:
-Changed name
-Changed maxpos and springs values
-Changed carbox and minor edits on texture

July 11, 2016:
-"0 0 0 RGB" fix on carbox


================================================================
=================== Permissions and terms ======================
================================================================

(This creation don't have a license. Even so, please be polite and respect the author wishes)

You're free to share (copy and redistribute the material in any medium or format) and adapt (remix, transform, and build upon the material) this creation, provided that:

1. You give appropriate credit and indicate if changes were made. You may do so in any reasonable manner.

2. You'll not use the material for commercial purposes.

3. If you remix, transform, or build upon the material, you must distribute your contributions under the same terms/permissions as the original.

4. You may not apply legal terms or technological measures that legally restrict others from doing anything the author permits.


================================================================
======================= Final notes ============================
================================================================

Please contact me if you find bugs or if you have any suggestion or idea. You can do so at the comments section of Re-Volt Zone or Dropbox, or even with a private message at the forum Re-Volt Live or via email.

My email adress: allanmoraes27@yahoo.com

Re-Volt Live profile: http://z3.invisionfree.com/Revolt_Live/index.php?s=90eae8a917757853e685db4ff40ec0a2&showuser=486



For more, visit my website: http://allanhqs.wix.com/warehouse


Have a nice Race!

Allan