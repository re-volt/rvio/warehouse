================================================================
Car Information
================================================================
Car Name  : Ford F-150 Lightning Pro
Car Type  : Conversion and Repaint
Folder	  : ...\cars\F150lightPro
Install   : Unzip with folder names on to the main ReVolt folder
Top speed : 45 mph
Rating    : Pro

================================================================
Author Information
================================================================
Author Name : Cat
EMail       : JZA80ToyotaSupraRZ@hotmail.com
Homepage    : http://www.freewebs.com/redpersiancat

================================================================
Construction
================================================================
Base           : Ford F-150 Lightning from Test Drive 6
                 by Pitbull Syndicate.
Poly Count     : ???
Editor(s) used : Ulead Photo Express 1.0, Zmodeler 1.07b,
                 RVshade, RV-sizer & prm2hul.
Known Bugs     : None

================================================================
Thanks And Accolades
================================================================
We want to thank you for downloading this car and all the
Re-Volt community for still playing this game.

================================================================
Copyright / Permissions
================================================================
Authors MAY use this Car as a base to build additional cars,
provided you give proper credit to the creators of the parts
from this car that were used to built the new car. E.g. if
you use the wheel mesh give me credit. 
 
You MAY distribute this CAR, provided you include this file,
with no modifications.  You may distribute this file in any
electronic format (BBS, Diskette, CD, etc) as long as you
include this file intact.

================================================================
Where to get this CAR
================================================================
Websites : www.rvzt.net
         : www.freewebs.com/redpersiancat

