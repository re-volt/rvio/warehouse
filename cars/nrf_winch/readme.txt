Both the body mesh and the texture of this file may only be used to play in this game. 
You may not use or modify the mesh or texture, or use it/distribute it anywhere else.