﻿Car information
================================================================
Car name                : Road Pirate
Car Type  		: Original
Top speed 		: 31.3 mph
Rating/Class   		: Rookie
Installed folder       	: ...\cars\fd6pirate

Author Information
================================================================
Author Name 		: Xarc
Email Address           : warrockrockwar@hotmail.it
 
Construction
================================================================
Base           		: Original
Editor(s) used 		: Blender and MediBang Paint Pro
 
Additional Credits 
================================================================
Credits to jigebren for the Blender plugin and prm2hul

Copyright / Permissions
================================================================
You may do whatever you want with this CAR.