August 06, 2017: Readme file of APC L-13 (Fan-Made).

================================================================
======================== Car Info ==============================
================================================================

Name				APC L-13
Author				Allan1
Date				October 30, 2012
Type				Original
Folder Name			lib13a
Rating				Advanced
Class				Glow
Drivetrain			Four-wheel Drive
Top speed			34 mph
Body mass			3.5 kg
Work time			12 hours


================================================================
========================= Setup ================================
================================================================

Extract the zipped file into your Re-Volt directory.


================================================================
======================= Description ============================
================================================================

That's a fan-made car based on the Re-Volt gallery picture lib13a.bmp: An old concept of car made by Paul Phippen. This picture is only available in the Dreamcast version of the game.

Please note that "APC L-13" is a fictitious name, not the real name of the car.


================================================================
======================== Credits ===============================
================================================================

Credits for the car's design comes to Paul Phippen.

Track in the picture is Aquaville by Hilaire9.

Tools:
Autodesk 3D Studio Max 2010
Asetools (by Ali and Kay)
Adobe Photoshop CS2
MS Paint
Re-Volt 1.2 (By Huki and Jig)
Prm2Hul (By Jig)
MS Notepad


================================================================
========================= Changelog ============================
================================================================

September 24, 2016:
-Remade the whole mesh
-Improved textures
-Remade carbox and shadow
-Changed parameters a bit

August 08, 2017:
-Fixed Center of Mass, body AngRes and springs Restitution.


================================================================
=================== Permissions and terms ======================
================================================================

(This creation don't have a license. Even so, please be polite and respect the author wishes)

You're free to share (copy and redistribute the material in any medium or format) and adapt (remix, transform, and build upon the material) this creation, provided that:

1. You give appropriate credit and indicate if changes were made. You may do so in any reasonable manner.

2. You'll not use the material for commercial purposes.

3. If you remix, transform, or build upon the material, you must distribute your contributions under the same terms/permissions as the original.

4. You may not apply legal terms or technological measures that legally restrict others from doing anything the author permits.


================================================================
======================= Final notes ============================
================================================================

Please contact me if you find bugs or if you have any suggestion or idea. You can do so at the comments section of Re-Volt Zone or Dropbox, or even with a private message at the forum Re-Volt Live or via email.

My email adress: allanmoraes27@yahoo.com

Re-Volt Live profile: http://z3.invisionfree.com/Revolt_Live/index.php?s=90eae8a917757853e685db4ff40ec0a2&showuser=486



For more, visit my website: http://allanhqs.wix.com/warehouse


Have a nice Race!

Allan