Car information
================================================================
Car name        	: BadJam

Car Type  		: Remodel/repaint/reparam

Top speed 		: 40mph (theoretical) / 66kph (observed)

Rating/Class   		: 4 (pro)

Installed folder       	: ...\cars\badjam

Description           	: This car is a remodel (wheels and springs) as well as a repaint and reparam of the famous Baja Bug
                        : Enjoy that big a** wheeled buggy !


Author Information
================================================================
Author Name 	: Z3R0L33T and thatmotorfreak (aka dbs213)

Email Address   : z3r0l33t@hotmail.fr

Other Info	: ReVolt user since 1999 !



Construction
================================================================
Base           	: Baja Bug by scloink

Editor(s) used 	: Blender, Paint.net, Photoshop



Additional Credits 
================================================================
Thanks to the whole community who continues to make ReVolt a fun and attractive game !
Special thanks to thatmotorfreak (aka dbs213) who did the car's whole repaint and helped me finish the params ! Great job once again dude !

BadJam = "Baja" & "Jam"



Copyright / Permissions
================================================================
You may do whatever you want with this car but you have to give us credits and give credit to the original author. Do not distribute without authorization.
 
