Prestonfs Autos
To the new owner of a cool 70's car:

This was a sudden idea that popped into my head today.
I thought:"What if I made a hatchback of a volken?"
So I did with a funny 1970's-esque styling to it by
adding a waterfall grille, and rallye stripes inspired
by the ford pinto. Hope you have a "Smashing" time with
this vehicle from Prestonfs Autos!

Thanks to Cat for his cool Volken Station Wagon Turbo,
The Ford Pinto for the inspiration of this car,
And of course Acclaim and the Re-Volt Community
for teaching me RV Shade and other things. July 8th 2008