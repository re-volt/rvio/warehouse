Car information
================================================================
Car name : Sol-Aire CX4
Car Type : Remodel/Reshade/Remap/Repaint/Reparam
Top speed : 41mph / 66kph
Rating/Class : 4 (pro)
Installed folder : ...\cars\cx4ii
Description : Modification of kirbyguy's Sol-Aire CX4, which was in turn converted from Hot Wheels Turbo Racing n64.

Has a high-ish top speed, but lacks grip and likes to spin out. Not good in packs or tight areas, but excels out in the open. Wedge-shaped front and low ride make for a fun time leisurely scooping up and tossing aside other cars, and the top speed enables it nicely, but watch your own back.

Comes with three skins:
Blue/Orange (HWTR style, similar to HW Race Team, '95)
Yellow/Stripes (Flyin' Aces, '98) (default)
Purple/Yellow (The Hot Ones, '11)


Author Information
================================================================
Author Name : Raster Gotolei
Email Address : rgotolei@gmail.com


Construction
================================================================
Base : kirbyguy22's Sol-Aire CX4  http://revoltzone.net/cars/2294/Sol-Aire%20CX4
Editor(s) used : blender for remodel/reshade/remap, gimp for skin/carbox, inkscape for skin details, gedit for text stuff


Additional Credits
================================================================
kirbyguy for thing that's been said two or three times already
Whoever did the modeling for HWTR
Jigebren for blender plugin
Marv for newer blender plugin
Speed Dreams for wheel texture on the yellow one


Copyright / Permissions
================================================================
You may do whatever you want with this car, just give credit where credit's due you know?
Repainters see misc folders, XCFs and a whole mess of other stuff in RVL thread download.
