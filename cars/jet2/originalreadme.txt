================================================================
Car name                : RV Jet Car
Install in folder       : Re-volt\cars\jet
Author                  : BurnRubr
Email Address           : 
Misc. Author Info       : This is only my 3rd car created from scratch for Re-Volt.

Description             : Example car to show that Gmax and Ultimate Unwrap 3D
                          can be used to create a Re-Volt car.

Additional Credits to   : Discreet for Gmax and Brad Bolthouse for Ultimate Unwrap3D
================================================================

* Play Information *

Top speed (observed)    : 48.8 mph
Rating                  : Pro
* Construction *
Base                    : New Car from scratch
Editor(s) used          : Gmax for the modelling.
                        : Model exported from Gmax as Quake 3 MD3 file
                        : Ultimate Unwrap 3D used to create the .prm file from the MD3 file

Known Bugs              : None Known


* Copyright / Permissions *

Authors MAY use this Car as a base to build additional cars.

You may do whatever you want with this CAR.

