Car information
================================================================
Car name : Spoiler Sport
Car Type : Original
Top speed : 33mph / 53kph
Rating/Class : 1 (amateur)
Installed folder : ...\cars\spoilersport
Description : Original model based on the Hot Wheels car of the same name.

Handles well on a clean track, albeit a touch slower than average. But when things get bumpy it goes wobbly. Can easily roll if pushed too far.
However, it's bulky and heavy. Bit of a bully. Expect to see an RC San or two launched off of the windshield at some point.

how do batteries work

Comes with three skins:
Bright Green (Original design, 1977)
Purple (The Hot Ones, 2011)
Blue with flames (The Hot Ones, 2012)

Different wheel model sets are included to match the different skins.
Green and Purple use bulgemix, Blue uses flatstretch. Simply copy/paste them around.


Author Information
================================================================
Author Name : Raster Gotolei
Email Address : rgotolei@gmail.com


Construction
================================================================
Base : Hot Wheels Spoiler Sport
Editor(s) used : Blender for model, Gimp for some parts of skin, Inkscape for other parts of skin, Xed for text stuff


Additional Credits
================================================================
Marv for blender plugin
Random ebay sellers for 10/10 reference images
Some guy on youtube who just uploads videos showing hot wheels cars on a turntable


Copyright / Permissions
================================================================
You may do whatever you want with this car, just give credit where credit's due you know?
XCFs and other stuff in RVL thread download.
