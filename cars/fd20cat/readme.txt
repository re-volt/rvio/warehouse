﻿Car information
================================================================
Car name                : Squcciant
Car Type  		: Original
Top speed 		: 32.7 mph
Rating/Class   		: Amateur
Installed folder       	: ...\cars\fd20cat

Author Information
================================================================
Author Name 		: Xarc
Email Address           : warrockrockwar@hotmail.it
 
Construction
================================================================
Base           		: Original
Editor(s) used 		: Blender and MediBang Paint Pro
 
Additional Credits 
================================================================
Credits to jigebren for the Blender plugin and prm2hul

Copyright / Permissions
================================================================
You may do whatever you want with this CAR.