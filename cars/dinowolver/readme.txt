Car information
================================================================
Car name                : RZ Fang
Car Type  		: Original
Top speed 		: 56 kph
Rating/Class   		: 2 (Advanced)
Installed folder       	: ...\cars\dinowolver
Description             : the light and agile continuous slider

RZ Fang is concept buggy based off RC San and Wolver from Scan2Go
the body design takes cues from the fastest accelerating amateur car in the game, redefining it with streamlined futuristic body.
painted with white and beige technological motif, the visuals of this car strikes sleekness and eye-catching interest.

the handling is a combination of RC San's lightness agility and Pole Poz' rail understeer.
RZ Fang's acceleration is almost the same as RC San, with its speed increased by a small margin.
this doesnt mean RZ Fang is better than its origin. at high speeds, the car suffers from understeer. 
gripping the car at low speeds is proven difficult as the power it outputs causes a loss of traction on the back.

the unique aspect of this car is the possibility of continuous powersliding. 
the car can maintain its powerslide indefinitely by holding the throttle while sliding.
the key to mastering the handling is figuring out the correct powersliding angles of each corner to take them on at full speed.


Author Information
================================================================
Author Name 		: shara coronvi
Email Address           : sharacolonvee@mail.com
Other Info		: pretty much a recent Re-Volt veteran. 
                          a good racer and drifter. experienced in parametering and repaints.
                          creator of aerodynamic car designs. also a drawing "artist" too.
 
Construction
================================================================
Base           		: Original
Editor(s) used 		: Notepad, Paint.NET, SketchUp and Blender
 
Additional Credits 
================================================================
Thanks and Credits to those who inspired or helped.

Acclaim for this great game
Microsoft for Notepad
Paint.NET developers for the afromentioned program
Marv for the Blender plugin
Mladen for the .hul collision file.
Kipy for linking me a wheel texture dump thread
FZG for GRID WHEELZ textures


Copyright / Permissions
================================================================
You may do whatever you want with this CAR.

however, if you're considering to put this car in a pack, you may NOT modify the car's handling.
if the car handling doesnt fit into the theme of the pack (ie too slow, too fast), simply dont put this car in.
though, you are ALLOWED to make a NEW car using this as a base, and then put that new car into a pack.
