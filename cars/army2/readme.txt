August 06, 2017: Readme file of Army Car.

================================================================
======================== Car Info ==============================
================================================================

Name				Army Car
Author				Allan1
Date				June 30, 2011
Type				Repaint
Folder name			army2
Rating				Advanced
Class				Glow
Drivetrain			Four-wheel Drive
Top speed			36 mph
Body mass			2.5 kg
Work time			4 hours and a half


================================================================
========================= Setup ================================
================================================================

Extract the zipped file into your Re-Volt directory. Simple as that.


================================================================
======================= Description ============================
================================================================

An army green-camouflage monster truck rc. Quite soft suspensions and sterring is a bit stiff.


================================================================
======================== Credits ===============================
================================================================

Wheels are from Jimk's Monster Dogde, and body is BigVolt by the game creators.

Track in the preview picture is Bone Island by Urne.

Tools:
GetUV (by Kay)
Re-Volt 1.2
Prm2Hul (By Jig)
Adobe Photoshop CS2
MS Paint
MS Notepad
Blender 2.66a

================================================================
======================== Changelog ===============================
================================================================

November 04, 2016:
-Improved graphics
-Improved parameters
-Added carbox and shadow

November 09, 2016:
-Added texture for the backwards (and edited wheels' mapping as well)
-Fixed colision's height

November 12, 2016:
-Changed hull's bottom
-Changed maxpos to 10


================================================================
=================== Permissions and terms ======================
================================================================

(This creation don't have a license. Even so, please be polite and respect the author wishes)

You're free to share (copy and redistribute the material in any medium or format) and adapt (remix, transform, and build upon the material) this creation, provided that:

1. You give appropriate credit and indicate if changes were made. You may do so in any reasonable manner.

2. You'll not use the material for commercial purposes.

3. If you remix, transform, or build upon the material, you must distribute your contributions under the same terms/permissions as the original.

4. You may not apply legal terms or technological measures that legally restrict others from doing anything the author permits.


================================================================
======================= Final notes ============================
================================================================

Please contact me if you find bugs or if you have any suggestion or idea. You can do so at the comments section of Re-Volt Zone or Dropbox, or even with a private message at the forum Re-Volt Live or via email.

My email adress: allanmoraes27@yahoo.com

Re-Volt Live profile: http://z3.invisionfree.com/Revolt_Live/index.php?s=90eae8a917757853e685db4ff40ec0a2&showuser=486



For more, visit my website: http://allanhqs.wix.com/warehouse


Have a nice Race!

Allan