Made by BloodBTF, april 2020

Car: RCS4
Rating: Advanced
Class: Electric
top speed: 34mph
accel: great
handling: great
Trans: 4WD
mass: 1.4kg

A good all round competetive race car. What it lacks in top speed it makes up for in acceleration and grip.

Fun Facts:
-design inspired by many 1/10 scale rc touring cars, namely the thunder tiger TS2e
-the name stands for R/C Sedan Four (wheel drive)

-paint kit included

You have my permission to do whatever you wish with this car, as long as they abide by the following rules
1. credit all apropriate authors if you re-release
2. you may not use this work for commercial purposes
