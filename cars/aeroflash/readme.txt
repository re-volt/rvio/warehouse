Car information
================================================================
Car name : Aeroflash
Car Type : Original
Top speed : 39 mph / 62 kph
Rating/Class : 4 (pro)
Installed folder : .../cars/aeroflash
Description : My second completed original. Based on the Hot Wheels model of the
same name. 

It gets off the line pretty quickly, but is far from the fastest of pros.
However it's quite easy to handle, provided you don't push it too hard and roll
it.

The wedged front sticks out just as far as it looks, so take care when hitting
sharp jumps.


Author Information
================================================================
Author Name : Raster Gotolei
Email Address : rgotolei@gmail.com


Construction
================================================================
Source : Aeroflash Hot Wheels model
Editor(s) used : blender for modeling/mapping, gimp for skin/carbox, inkscape
for flames and wheels, pluma for text stuff


Additional Credits
================================================================
Larry Wood for designing the original vehicle


Copyright / Permissions
================================================================
You may do whatever you want with this car, just have some courtesy you know?
XCFs/SVGs in RVL thread download.
